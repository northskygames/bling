﻿//
// V1.17
//
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.Events;

public class Sequence : MonoBehaviour
{
    #region VARIABLES

    [Space(10)]
    [Header("NOTE: SPRITE SHEET LOCATION /RESOURCES/SPRITES")]
    [Header("-------------")]
    [Space(-10)]
    [Header("SEQUENCE V1.17")]

    [Tooltip("Fade between frames; fade-out current frame and at the same time fade-in the next frame")]
    public bool frameBlend;
    [Tooltip("Flip the sequence at random (X) upon initialisation")]
    public bool frameRandomX;
    [Tooltip("Flip the sequence at random (Y) upon initialisation")]
    public bool frameRandomY;
    [Tooltip("Rotate / Flip the sequence around the Z axis")]
    public float frameRotate = 0;
    [Tooltip("Select an alternate material to use with this sequence. None = Default")]
    public Material frameMaterial;
    [Tooltip("Use a sprite sheet rather than individual images")]
    public Sprite frameSheet;
    [Tooltip("Specify a location for the spritesheet other than the default")]
    public string spriteLocation = "Sprites/";
    [Tooltip("Use individual images instead of a sprite sheet")]
    public Sprite[] frames;
    [Space(10)]
    [Tooltip("The current clip that displays upon launch. You can also change this value to directly switch to a different clip whilst running")]
    public int clipActiveUpdater;
    [Tooltip("Displays the current clip that is active / playing")]
    public int clipActive;
    [Tooltip("Play a random clip when activated")]
    public bool clipRandom;
    [Tooltip("Use the clip strip to link various clips together as one long animation")]
    [TextArea(2, 2)]
    public string clipStrip;
    private List<float> clipStripList = new List<float>();
    private int clipStripIndex;

    [System.Serializable]
    public class Clip
    {
        [Tooltip("Clip identifier. Give this sequence a name, example; chr-laugh, look-up, happy-face. Default name given if left blank")]
        public string clipName;
        [Tooltip("Add a delay between clip loops; 0 = none, N = delay and -N = random delay (between 0 > N)")]
        public float clipDelay;
        [Tooltip("The frame list. Note use '-' to adjust the playback time. Example; '1,2,-0.01,3,4' ... play frames 1 > 2 > change time > 3 > 4")]
        [TextArea(2, 2)]
        public string clipFrames;
        [HideInInspector]
        public List<float> clipFrameList;
        [Tooltip("Default animation speed")]
        public float clipSpeed;
        [Tooltip("Set the start frame of this clip; 0 = begining, N = place in sequence and -1 = random")]
        public int clipStart;
        [Tooltip("Animate clip once or loop")]
        public bool clipLoop;
        [Tooltip("Once completed switch to another clip")]
        public string clipSwitch;
        [Tooltip("Change this clips colour based on parent; 0 = no change, 1 = set colour upon spawn only, 2 = set / update colour every time the clips frame index returns to the start")]
        [RangeAttribute(0, 2)]
        public int clipColour;
        [HideInInspector]
        public int clipIndex;
        [HideInInspector]
        public bool clipRandomDelay;
    }

    [Space(10)]
    [Tooltip("Clips are collections of individual images; an animation sequence")]
    public List<Clip> clip = new List<Clip>();
    [HideInInspector]
    public List<Clip> clipCopy = new List<Clip>();

    public class SequenceObj
    {
        public GameObject obj;
        public float fadeRate;
        public float alpha;
    }
    [HideInInspector]
    public List<SequenceObj> sequenceobj = new List<SequenceObj>();

    private float currentFps;
    private int activeSequence;
    private bool initialized = false;
    private bool active = false;
    private bool isRunning = true;
    private bool timersCalculated = false;
    public UnityEvent finishedAnimating;

    #endregion

    public bool Active //Public accessor for active bool
    {
        get { return active; }
        set { active = value; }
    }

    private void OnValidate()
    {
        // This can be used to report / act on any changes within the inspector
    }

    void OnEnable()
    {
        StartCoroutine(KickStart());
    }

    void ErrorOut(string e)
    {
        Debug.LogWarning("SEQUENCE " + e);

        if (sequenceobj.Count > 0)
        {
            foreach (SequenceObj n in sequenceobj)
                Destroy(n.obj);
        }

        active = false;
        GetComponent<Sequence>().enabled = false;
    }

    float UpdateFrameSpeed(float s)
    {
        return s * currentFps;
    }

    float UpdateSequenceObjFadeRate()
    {
        float s = clip[clipActive].clipSpeed;
        float n = (1.0f / s) * 100f;

        return n * 0.01f;
    }

    void TranscribeFrameStrings()
    {
        for (int i = 0; i < clip.Count; i++)
        {
            if (clip[i].clipFrames.Length < 2)
            {
                ErrorOut("missing frames");
                break;
            }
                
            string[] splitArray = clip[i].clipFrames.Split(char.Parse(","));

            for (int n = 0; n < splitArray.Length; n++)
            {
                if (!float.TryParse(splitArray[n], out float f))
                {
                    ErrorOut("bad value in frame list");
                    break;
                }
                else if(f > frames.Length - 1)
                {
                    ErrorOut("frame list value out of bounds");
                    break;
                }
                else
                {
                    clip[i].clipFrameList.Add(f);
                }
            }
        }
    }

    void TranscribeClipStrip()
    {
        if (clipStrip.Length < 2)
            return;

        string[] splitArray = clipStrip.Split(char.Parse(","));

        for (int n = 0; n < splitArray.Length; n++)
        {
            if (!float.TryParse(splitArray[n], out float f))
            {
                ErrorOut("bad value in clip list");
                break;
            }
            else if (f < 0)
            {
                ErrorOut("clip list value cannot be negative");
                break;
            }
            else if (f > clip.Count - 1)
            {
                ErrorOut("clip list value out of bounds");
                break;
            }
            else
            {
                clipStripList.Add(float.Parse(splitArray[n]));
            }
        }

        if (clipStripList.Count > 0)
        {
            clipActive = (int)clipStripList[0];
            clipActiveUpdater = clipActive;
        }
    }

    void GetTimers()
    {
        timersCalculated = true;
        float speed;
        currentFps = 1.0f / Time.fixedDeltaTime;

        for (int i = 0; i < clip.Count; i++)
        {
            Clip c = clip[i];
            speed = c.clipSpeed * currentFps;
            c.clipSpeed = speed;
            speed = c.clipDelay * currentFps;
            c.clipDelay = speed;
        }
    }

    IEnumerator KickStart()
    {
        // wait 1 frame
        //
        yield return null;

        if (!initialized)
        {
            activeSequence = 0;
            clipStripIndex = 0;

            if (clipRandom)
            {
                clipActive = Random.Range(0, clip.Count);
                clipActiveUpdater = clipActive;
            }

            // if a sprite sheet is provided then grab the frames from it
            //
            if (string.IsNullOrEmpty(spriteLocation))
                spriteLocation = "Sprites/";

            if (frameSheet != null)
                frames = Resources.LoadAll<Sprite>(spriteLocation + frameSheet.texture.name);

            CreateSequenceObjects();
            TranscribeFrameStrings();
            TranscribeClipStrip();
            if (!timersCalculated)
                GetTimers();

            // make a backup of the clip list and all its contents
            //
            clipCopy = new List<Clip>();

            foreach (var i in clip)
            {
                clipCopy.Add(new Clip
                {
                    clipName = i.clipName,
                    clipDelay = i.clipDelay,
                    clipFrames = i.clipFrames,
                    clipFrameList = i.clipFrameList,
                    clipSpeed = i.clipSpeed,
                    clipStart = i.clipStart,
                    clipLoop = i.clipLoop,
                    clipSwitch = i.clipSwitch,
                    clipColour = i.clipColour
                }) ;
            }

            //
            //
            for (int i = 0; i < clip.Count; i++)
            {
                if (clip[i].clipSpeed <= 0)
                {
                    ErrorOut("bad clip speed");
                    break;
                }

                // negative delay = random delay
                //
                if (clip[i].clipDelay < 0)
                {
                    clip[i].clipRandomDelay = true;
                    clipCopy[i].clipRandomDelay = true;
                    clip[i].clipDelay = -GetRandomDelay(clipCopy[i].clipDelay);
                }
                else
                {
                    clip[i].clipRandomDelay = false;
                    clipCopy[i].clipRandomDelay = false;
                }

                if (clip[i].clipStart < -1 || clip[i].clipStart > clip[i].clipFrameList.Count - 1)
                {
                    ErrorOut("animation start out of range");
                    break;
                }

                // assign start frame -1 = random
                //
                if (clip[i].clipStart < 0)
                {
                    clip[i].clipStart = 0;
                    clip[i].clipIndex = Random.Range(0, clip[i].clipFrameList.Count - 1);
                }
                else
                {
                    clip[i].clipIndex = clip[i].clipStart;
                }

                clipCopy[i].clipIndex = clip[i].clipStart;

                // update the sequence object image with the currently active clips frameindex
                //
                if (i == clipActive)
                {
                    AssignSpriteFrame(i, activeSequence);

                    if (frameBlend)
                    {
                        IncrementSequence();
                        IncrementFrame();
                        AssignSpriteFrame(i, activeSequence);
                    }
                }

                // assign default name to a clip if field is empty
                //
                if (clip[i].clipName == "")
                    clip[i].clipName = "Clip " + i.ToString("000");

                clipCopy[i].clipName = clip[i].clipName;
            }

            initialized = true;
            active = true;
            isRunning = true;
        }
        else
        {
            // to stop frame skipping, backup the current index then resore it after resetting the clips values
            //
            // Debug.Log("restoring " + clipActive);

            int clipindexcopy = clip[clipActive].clipIndex;
            RestoreClip(clip[clipActive]);
            clip[clipActive].clipIndex = clipindexcopy;

            active = true;
            isRunning = true;
        }

        //
        //

        //
        // UPDATE CURRENT OBJECTS COLOUR
        //

        //
        //
        if (clip[clipActive].clipColour > 0)
            sequenceobj[clipActive].obj.GetComponent<Image>().color = GetComponent<Image>().color;
    }

    float GetRandomDelay(float d)
    {
        return Random.Range(d * 0.25f, d);
    }

    void CreateSequenceObjects()
    {
        GameObject obj;

        int n = 1;

        if (frameBlend)
            n = 2;

        for (int i = 0; i < n; i++)
        {
            obj = new GameObject("animator" + sequenceobj.Count.ToString());
            obj.AddComponent<Image>();
            obj.transform.SetParent(gameObject.transform, true);
            obj.transform.rotation = Quaternion.Euler(new Vector3 (0, 0, frameRotate));
            obj.transform.localScale = new Vector3(1, 1, 1);
            obj.transform.localPosition = Vector2.zero;
            obj.GetComponent<RectTransform>().pivot = GetComponent<RectTransform>().pivot;
            obj.GetComponent<RectTransform>().sizeDelta = GetComponent<RectTransform>().rect.size;
            obj.GetComponent<Image>().material = frameMaterial;

            if (frameRandomX && Random.Range(0, 2) == 0)
                obj.transform.rotation = Quaternion.Euler(new Vector3(0, 180, 0));

            if (frameRandomY && Random.Range(0, 2) == 0)
                obj.transform.rotation = Quaternion.Euler(new Vector3(180, 0, 0));

            obj.SetActive(true);

            //
            //

            //
            // UPDATE CURRENT OBJECTS COLOUR
            //

            //
            //
            if (i == 0)
            {
                if (clip[clipActive].clipColour > 0)
                    obj.GetComponent<Image>().color = UpdateAnmObjColor(GetComponent<Image>().color, 1);
                else
                    obj.GetComponent<Image>().color = UpdateAnmObjColor(obj.GetComponent<Image>().color, 1);
            }
            else
            {
                if (clip[clipActive].clipColour > 0)
                    obj.GetComponent<Image>().color = UpdateAnmObjColor(GetComponent<Image>().color, 0);
                else
                    obj.GetComponent<Image>().color = UpdateAnmObjColor(obj.GetComponent<Image>().color, 0);
            }

            sequenceobj.Add(new SequenceObj { obj = obj, fadeRate = 0, alpha = 0f });
        }
    }

    Color UpdateAnmObjColor(Color color, float alpha)
    {
        Color colorout = new Color(color.r, color.g, color.b, alpha);
        return colorout;
    }

    void AssignSpriteFrame(int clipi, int sequencei)
    {
        sequenceobj[sequencei].obj.GetComponent<Image>().sprite = frames[(int)clip[clipi].clipFrameList[clip[clipi].clipIndex]];
        sequenceobj[sequencei].obj.GetComponent<Image>().SetNativeSize();
    }

    void FixedUpdate()
    {
        if (active)
        {
            bool animateFrame = true;

            if (clip[clipActive].clipDelay > 0)
            {
                clip[clipActive].clipDelay--;
                animateFrame = false;
            }

            if (clipActive != clipActiveUpdater)
            {
                RestoreClip(clip[clipActive]);
                clipActive = clipActiveUpdater;
                clip[clipActive].clipDelay = 0;
                animateFrame = true;
            }

            if (animateFrame)
            {
                HandleAnimate();

                if (frameBlend)
                    HandleSequenceObjects();
            }
        }
        else if (!isRunning)
        {
            this.enabled = false;
        }
    }

    void HandleSequenceObjects()
    {
        foreach (SequenceObj s in sequenceobj)
        {
            if ((s.alpha > 0 && s.fadeRate < 0) || (s.alpha < 1f && s.fadeRate > 0))
            {
                s.alpha += s.fadeRate;
                //
                //

                //
                // UPDATE OBJECTS ALPHA
                //

                //
                //
                s.obj.GetComponent<Image>().color = UpdateAnmObjColor(s.obj.GetComponent<Image>().color, s.alpha);
            }
        }
    }

    void IncrementSequence()
    {
        sequenceobj[activeSequence].fadeRate = -UpdateSequenceObjFadeRate();

        activeSequence++;

        if (activeSequence > sequenceobj.Count - 1)
            activeSequence = 0;

        sequenceobj[activeSequence].fadeRate = UpdateSequenceObjFadeRate();
    }

    public void ShutDown()
    {
        if (clipActive > 0 && clipActive < clip.Count)
            RestoreClip(clip[clipActive]);

        clipActive = 0;
        clipActiveUpdater = clipActive;
        activeSequence = 0;
        clipStripIndex = 0;
        isRunning = false;
        active = false;
        initialized = false;

        foreach (SequenceObj s in sequenceobj)
        {
            Destroy(s.obj);
        }
        sequenceobj.Clear();

        StopAllCoroutines();

        this.enabled = false;
    }

    public void Reset()
    {
        clipActive = 0;
        clipActiveUpdater = 0;
        activeSequence = 0;
        clipStripIndex = 0;
        isRunning = true;
        active = true;
    }

    void RestoreClip(Clip c)
    {
        Clip r = clipCopy[clipActive];

        if (c.clipRandomDelay)
        {
            c.clipDelay = -GetRandomDelay(r.clipDelay);
        }
        else
        {
            c.clipDelay = r.clipDelay;
        }

        c.clipRandomDelay = r.clipRandomDelay;
        c.clipIndex = r.clipIndex;
        c.clipFrames = r.clipFrames;
        c.clipFrameList = r.clipFrameList;
        c.clipSpeed = r.clipSpeed;
        c.clipStart = r.clipStart;
        c.clipLoop = r.clipLoop;
        c.clipSwitch = r.clipSwitch;
        c.clipColour = r.clipColour;

        //
        //

        //
        // UPDATE CURRENT OBJECTS COLOUR
        //

        //
        //
        if (clip[clipActive].clipColour > 0)
        {
            sequenceobj[activeSequence].obj.GetComponent<Image>().color = UpdateAnmObjColor(GetComponent<Image>().color, GetComponent<Image>().color.a);
        }
    }

    void IncrementFrame()
    {
        Clip c = clip[clipActive];

        c.clipIndex++;

        // end of clip frame list check
        //
        if (c.clipIndex > c.clipFrameList.Count - 1)
        {
            RestoreClip(c);

            if (!c.clipLoop)
            {
                if (c.clipSwitch != "")
                {
                    clipActive = int.Parse(c.clipSwitch);
                    clipActiveUpdater = clipActive;
                }
                else if (clipStripList.Count > 0)
                {
                    clipStripIndex++;

                    if (clipStripIndex > clipStripList.Count - 1)
                    {
                        active = false;
                        isRunning = false;
                        finishedAnimating.Invoke();
                    }
                    else
                    {
                        clipActive = (int)clipStripList[clipStripIndex];
                        clipActiveUpdater = clipActive;
                    }
                }
                else
                {
                    active = false;
                    isRunning = false;
                    finishedAnimating.Invoke();
                }
            }
        }
    }

    void HandleAnimate()
    {
        Clip c = clip[clipActive];

        c.clipSpeed--;

        if (c.clipSpeed <= 0)
        {
            IncrementFrame();

            if (!active)
                return;

            // negative cliplist value check; change frame speed
            //
            if (c.clipFrameList[c.clipIndex] < 0)
            {
                c.clipSpeed = UpdateFrameSpeed(-c.clipFrameList[c.clipIndex]);
                IncrementFrame();
            }
            else
            {
                c.clipSpeed = clipCopy[clipActive].clipSpeed;
            }

            if (active)
            {
                if (frameBlend)
                    IncrementSequence();

                AssignSpriteFrame(clipActive, activeSequence);
            }
        }
    }
}