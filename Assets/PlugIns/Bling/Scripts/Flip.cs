//
// V1.0
//
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class Flip : MonoBehaviour
{
	private Vector3 scaleBup;
	private bool initialized = false;
	private float flipSpeed = 0.075f;
	private float startScale;
	private float targetScale;
	private float time;

	public Transform frontFace;
	public Transform backFace;

	public UnityEvent flipped;

	public enum flipStates
	{
		Idle,
		FlipIn,
		FlipOut
	}
	public flipStates currentState = flipStates.Idle;

	void OnEnable()
	{
		StartCoroutine(Delay());
	}

	void OnDisable()
	{
		initialized = false;
	}

	IEnumerator Delay()
	{
		yield return new WaitForEndOfFrame();
		if (!initialized)
		{
			KickStart();
			initialized = true;
		}
	}

	void KickStart()
    {
		scaleBup = transform.localScale;
		startScale = scaleBup.x;
		targetScale = 0f;
		currentState = flipStates.FlipOut;
	}

	void SetScale()
    {
		if (currentState == flipStates.FlipOut)
		{
            frontFace.gameObject.SetActive(!frontFace.gameObject.activeSelf);
            backFace.gameObject.SetActive(!frontFace.gameObject.activeSelf);

            startScale = 0f;
			targetScale = scaleBup.x;
			currentState = flipStates.FlipIn;
		}
        else
        {
			startScale = scaleBup.x;
			targetScale = 0f;
			currentState = flipStates.Idle;
			flipped.Invoke();
		}

		time = 0;
		transform.localScale = new Vector3(startScale, scaleBup.y, scaleBup.z);
	}

	void FixedUpdate()
	{
		if (currentState != flipStates.Idle)
		{
			if (Vector3.Distance(transform.localScale, new Vector3(targetScale, scaleBup.y, scaleBup.z)) > 0.05f)
            {
				time += Time.deltaTime;
				float scaleX = Mathf.Lerp(startScale, targetScale, time / flipSpeed);
				transform.localScale = new Vector3(scaleX, scaleBup.y, scaleBup.z);
			}
            else
			{
				SetScale();
			}
		}
	}
}