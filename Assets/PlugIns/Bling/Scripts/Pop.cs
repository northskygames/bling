﻿//
// V1.7
//
using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class Pop : MonoBehaviour
{
	#region VARIABLES
	//
	// set the overall speed of the intro / outro here, this value also drives the fade in / out
	//
	const float bumpSpeed = 0.35f;
	const float fadeSpeed = 0.35f;
	const float defaultAlpha = 1;

	private Vector3 posStart;
	private Vector3 posMid;
	private Vector3 posEnd;
	private Vector3 posTarget;

	private float fadeTimeSet;
	private float currentLerpTime;

	public enum ScrollDirections
	{
		None = 0,
		Up = 1,
		Down = 2,
		Left = 3,
		Right = 4
	}

    [Space (10)]
    public Transform fadeTransform;
    public Transform bumpTransform;

	[Space (10)]
	public ScrollDirections scrollInDirection;
	public ScrollDirections scrollOutDirection;

	public bool bumpInActive;
	public bool bumpOutActive;

	// v1.7 updated the following to FALSE
	//
	private bool fadeInActive = false;
	private bool fadeOutActive = false;
	private bool busy = false;

	[Space (10)]
	public UnityEvent bumpInComplete;
	public UnityEvent bumpOutComplete;

	private CanvasGroup fadeCanvasGroup;
    private CanvasGroup bumpCanvasGroup;
	private bool initialized = false;
	#endregion

	void OnEnable ()
	{
		StartCoroutine (Delay ());
	}

	void OnDisable ()
	{
		initialized = false;
	}

	void InitializeCanvas () 
	{
		// bumps
		//
        if (bumpTransform == null)
            bumpTransform = transform;

        if (bumpTransform != null)
            bumpCanvasGroup = bumpTransform.GetComponent<CanvasGroup>();

		if (bumpCanvasGroup == null)
			Debug.LogWarning ("Pop: Missing Canvas on " + bumpTransform.name);

		// fades
		//
        if (fadeTransform != null)
            fadeCanvasGroup = fadeTransform.GetComponent<CanvasGroup>();

		if (fadeCanvasGroup == null && fadeTransform != null)
			Debug.LogWarning ("Pop: Missing Canvas on " + fadeTransform.name);

		if (fadeCanvasGroup != null)
			fadeCanvasGroup.alpha = 0;

        if (bumpCanvasGroup != null)
            bumpCanvasGroup.alpha = 0;
	}

	IEnumerator Delay() 
	{
		if (!busy)
			InitializeCanvas ();
		
		yield return new WaitForEndOfFrame();

		if (!initialized)
		{
			KickStart ();
			initialized = true;
		}
	}

	public void BumpIn () 
	{
		//
		// interupt fade out with a fade in
		//
		if (fadeOutActive)
        {
			fadeOutActive = false;
			fadeInActive = true;
			busy = true;
			return;
		}

		currentLerpTime = 0f;
		posTarget = posMid;

		if (fadeCanvasGroup != null)
			fadeInActive = true;
		else
			fadeInActive = false;

		bumpInActive = true;
		busy = true;
	}

	public void BumpOut ()
	{
		//
		// interupt fade out with a fade in
		//
		if (fadeInActive)
		{
			fadeOutActive = true;
			fadeInActive = false;
			busy = true;
			return;
		}

		currentLerpTime = 0f;
		posStart = bumpTransform.localPosition;
		posTarget = posEnd;

		if (fadeCanvasGroup != null)
			fadeOutActive = true;
		else
			fadeOutActive = false;

		bumpOutActive = true;
		busy = true;
	}

	void KickStart () 
	{
        Vector2 rectsize = bumpTransform.parent.gameObject.GetComponent<RectTransform>().rect.size * 0.5f;
		rectsize += GetComponent<RectTransform>().rect.size * 0.5f;
		fadeTimeSet = fadeSpeed;
        posStart = bumpTransform.localPosition;
        posMid = bumpTransform.localPosition;
        posEnd = bumpTransform.localPosition;

		// get start position for scene
		//
		if (scrollInDirection == ScrollDirections.Left) 
		{
			posStart.x = rectsize.x;
		} else if (scrollInDirection == ScrollDirections.Right)
		{
			posStart.x = -(rectsize.x);
		}
		if (scrollInDirection == ScrollDirections.Down)
		{
			posStart.y = rectsize.y;
		} else if (scrollInDirection == ScrollDirections.Up) 
		{
			posStart.y = -(rectsize.y);
		}

		// get end position for scene
		//
		if (scrollOutDirection == ScrollDirections.Left)
		{
			posEnd.x = -rectsize.x;
		} else if (scrollOutDirection == ScrollDirections.Right) 
		{
			posEnd.x = rectsize.x;
		}
		if (scrollOutDirection == ScrollDirections.Down) 
		{
			posEnd.y = -rectsize.y;
		} else if (scrollOutDirection == ScrollDirections.Up) 
		{
			posEnd.y = rectsize.y;
		}

		//
		//
		if (fadeCanvasGroup != null)
		{
			fadeCanvasGroup.alpha = 0;
		}

		// start = current pos + / - offset, mid = arrival point, end = end point
		//
        if (bumpTransform != null)
            bumpTransform.localPosition = posStart;

        if (bumpCanvasGroup != null)
            bumpCanvasGroup.alpha = 1;

		//
		//
		ConvertTimers ();

		//
		//
		BumpIn ();
	}

	void FixedUpdate () 
	{
		if (busy)
			HandleBump ();
	}

	void HandleBump () 
	{
		if (fadeCanvasGroup != null)
		{
			if (fadeInActive)
			{
				if (fadeCanvasGroup.alpha < defaultAlpha)
				{
					fadeCanvasGroup.alpha += fadeTimeSet;
				} else
				{
					fadeInActive = false;
				}
			} else if (fadeOutActive)
			{
				if (fadeCanvasGroup.alpha > 0)
				{
					fadeCanvasGroup.alpha -= fadeTimeSet;
				} else 
				{
					fadeOutActive = false;
				}
			}
		}

        if (Vector3.Distance (bumpTransform.localPosition, posTarget) > 1f)
		{
			currentLerpTime += Time.deltaTime;

			if (currentLerpTime > bumpSpeed) 
				currentLerpTime = bumpSpeed;
			
			float n = currentLerpTime / bumpSpeed;
			// ease out; n = Mathf.Sin(n * Mathf.PI * 0.5f);
			// ease in; n = 1f - Mathf.Cos(n * Mathf.PI * 0.5f)
			//
			n = Mathf.Sin(n * Mathf.PI * 0.5f);
            bumpTransform.localPosition = Vector3.Lerp(posStart, posTarget, n);
		} else
		{
			if (!fadeInActive && !fadeOutActive) 
			{
				busy = false;
				if (bumpInActive) 
				{
					bumpInActive = false;
					bumpInComplete.Invoke();
				} else if (bumpOutActive)
				{
					bumpOutActive = false;
					initialized = false;

					// reset the scenes position to its original, incase we want to re-trigger this object and have it manipulate the scene again
					//
                    bumpTransform.localPosition = posMid;
					bumpOutComplete.Invoke();
				}
			}
		}
	}

	void ConvertTimers () 
	{
		float currentfps = 1.0f / Time.fixedDeltaTime;
		float frametotal = fadeTimeSet * currentfps;
		fadeTimeSet = 1f / frametotal;
	}
}