﻿//
// V1.4
//
using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class Bouncer : MonoBehaviour
{
	#region VARIABLES

	private Vector3 BounceMaxSet;
	private float BounceSpeedSet;
	private float BounceRestartDelaySet;
	private Vector3 Scale = new Vector3(1, 1, 1);
	private float CurrentFps;
	private bool StoredUserSettings = false;
	private bool initialized = false;

	// v1.4 updated the following to FALSE
	//
	private bool Bouncing = false;

	public Vector3 BounceMax;
	[RangeAttribute(0, 25)]
	public float BounceSpeed;
	[RangeAttribute(0, 1000)]
	public float BounceRestartDelay;
	public bool RandomBounceDelay;
	public bool BurstBounce;
	[Space(10)]
	public bool Active;
	public UnityEvent StartedMoving;
	public UnityEvent StoppedMoving;

	const float rndhi = 1.5f;
	const float rndlo = 0.5f;

	#endregion

	void OnEnable()
	{
		StartCoroutine(Delay());
	}

	void OnDisable()
	{
		initialized = false;
	}

	IEnumerator Delay()
	{
		yield return new WaitForEndOfFrame();
		if (!initialized)
		{
			KickStart();
			initialized = true;
		}
	}

	void KickStart()
	{
		CurrentFps = 1 / Time.fixedDeltaTime;

		// record the current scale of the object so we can reset it back later after bouncing (rather than what it did before, which was reset its scale back to 1,1,1)
		//
		Scale = transform.localScale;

		if (!StoredUserSettings)
		{
			BounceMaxSet = BounceMax;
			BounceSpeedSet = BounceSpeed;
			BounceRestartDelaySet = BounceRestartDelay;
			StoredUserSettings = true;
		}
		BounceMax = BounceMaxSet;
		BounceSpeed = BounceSpeedSet * CurrentFps;
		BounceRestartDelay = BounceRestartDelaySet * CurrentFps;

		if (RandomBounceDelay)
			BounceRestartDelay = (int)Random.Range(BounceRestartDelaySet * rndlo, BounceRestartDelaySet * rndhi) * CurrentFps;

		Bouncing = true;
		Active = true;
		StartedMoving.Invoke();
	}

	void FixedUpdate()
	{
		if (Active)
			HandleMover();
	}

	void HandleMover()
	{
		if (Active)
		{
			if (Bouncing)
			{
				if (Vector3.Distance(BounceMax, new Vector3(0, 0, 0)) > 0.5f)
				{
					if (BounceSpeed-- <= 0)
					{
						BounceSpeed = BounceSpeedSet * CurrentFps;
						BounceMax *= -1 * 0.75f;
						transform.localScale = new Vector3(Scale.x + (BounceMax.x * 0.05f), Scale.y + (BounceMax.y * 0.05f), Scale.z + (BounceMax.z * 0.05f));
					}
				}
				else
				{
					if (BurstBounce)
					{
						Active = false;
					}
					Bouncing = false;
					transform.localScale = Scale;
					StoppedMoving.Invoke();
				}
			}
			else if (BounceRestartDelay-- <= 0)
			{
				KickStart();
			}
		}
	}
}