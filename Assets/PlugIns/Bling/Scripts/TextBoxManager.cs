﻿//
// V1.0
//
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextBoxManager : MonoBehaviour
{
    public static TextBoxManager instance;
    void Awake()
    {
        if (instance != null)
            Destroy(gameObject);
        else
            instance = this;
    }

    private class TextObject
    {
        public float pausetimer;
        public Vector3 targetxy;
        public GameObject textobj;
        public bool targetset;
        public bool initialized;
    }

    [Space(10)]
    public Font textFont;
    public Color textColor;
    public int textSize;
    [Header("Align Center ON by default")]
    public bool alignLeft;
    public bool alignRight;
    [Header("Vertical Scroll and Text Pause")]
    public float scrollSpeed;
    public float pauseTimer;
    [Space (10)]
    public bool active;

    private float boxHeight;
    private float moveDistance;
    private float currentfps;

    readonly List<TextObject> objList = new List<TextObject>();
    readonly List<TextObject> dellist = new List<TextObject>();

    void Start()
    {
        currentfps = 1.0f / Time.fixedDeltaTime;
        boxHeight = (this.GetComponentInParent<RectTransform>().sizeDelta.y / 2) + (textSize / 2);

        // if box mod text = 0; allow for multiple lines of text otherwise set text to scroll into the center of the box
        //
        if (((int)this.GetComponentInParent<RectTransform>().sizeDelta.y % textSize) == 0)
        {
            moveDistance = textSize;
        }
        else
        {
            moveDistance = boxHeight;
        }

        scrollSpeed = Vector3.Distance(new Vector2(0, 0), new Vector2(0, moveDistance)) / (scrollSpeed * currentfps);
        pauseTimer *= currentfps;
    }

    public void AddTextToList (string textIn)
    {
        objList.Add(InitializeTextObject(textIn));
    }

    TextObject InitializeTextObject(string textOut)
    {
        TextObject newnode = new TextObject
        {
            textobj = CreateTextLine(textOut),
            pausetimer = pauseTimer,
            targetxy = new Vector2 (0, 0),
            targetset = false,
            initialized = false
        };

        return newnode;
    }

    GameObject CreateTextLine(string textout)
    {
        // define text properties
        //
        GameObject obj = new GameObject("objText");
        obj.AddComponent<Text>();
        Text t = obj.GetComponent<Text>();
        t.text = textout;
        t.font = textFont;
        t.fontSize = textSize;
        t.color = textColor;
        t.verticalOverflow = VerticalWrapMode.Overflow;
        t.horizontalOverflow = HorizontalWrapMode.Overflow;

        if (alignLeft)
        {
            t.alignment = TextAnchor.MiddleLeft;
        }
        else if (alignRight)
        {
            t.alignment = TextAnchor.MiddleRight;
        }
        else
        {
            t.alignment = TextAnchor.MiddleCenter;
        }

        obj.transform.SetParent(this.transform, true);
        obj.transform.localScale = new Vector3(1, 1, 1);
        obj.transform.localPosition = new Vector2(0, -(textSize / 2));
        obj.transform.GetComponent<RectTransform>().sizeDelta = new Vector2 (this.GetComponentInParent<RectTransform>().sizeDelta.x, textSize);
        obj.transform.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);
        obj.transform.GetComponent<RectTransform>().anchorMin = new Vector2(0.5f, 0);
        obj.transform.GetComponent<RectTransform>().anchorMax = new Vector2(0.5f, 0);

        obj.SetActive(true);

        return obj;
    }

    void Update()
    {
        if (objList.Count > 0)
        {
            bool busy = false;
            active = true;

            foreach (TextObject node in objList)
            {
                if (node.initialized)
                {
                    busy = true;

                    Transform t = node.textobj.transform;

                    if (!node.targetset)
                    {
                        node.targetxy = new Vector3(t.localPosition.x, t.localPosition.y + moveDistance, 0);
                        node.targetset = true;
                    }

                    if (Vector3.Distance(t.localPosition, node.targetxy) > 1f && node.targetset)
                    {
                        t.localPosition = Vector3.MoveTowards(t.localPosition, node.targetxy, scrollSpeed);

                        if (t.localPosition.y > boxHeight)
                            dellist.Add(node);
                    }
                    else
                    {
                        if (node.pausetimer > 0)
                        {
                            node.pausetimer--;
                        }
                        else
                        {
                            node.pausetimer = pauseTimer;
                            node.targetset = false;
                            busy = false;
                        }
                    }
                }
                
                if (!node.initialized)
                {
                    if (!busy)
                    {
                        node.initialized = true;
                        busy = true;
                    }
                }
            }
                
            if (dellist.Count > 0)
            {
                foreach (TextObject node in dellist)
                {
                    Destroy(node.textobj);
                    objList.Remove(node);
                }
                dellist.Clear();
            }

            if (objList.Count < 1)
                active = false;
        }
    }
}
