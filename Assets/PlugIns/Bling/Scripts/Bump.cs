﻿//
// V1.4
//
using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class Bump : MonoBehaviour 
{
	#region VARIABLES
	[System.Serializable]
	public class clsBump
	{
		[System.NonSerialized]
		public Vector2 ObjSize;
		[Header("force bump to start and end off screen")]
		public GameObject SourceScene;
		public GameObject SourceObject;
		[Header("bump distance [POINTS]")]
		public Vector2 BumpInOffset;
		public Vector2 BumpOutOffset;
		[Space(10)]
		public bool NoLerp;
		public bool TriggerBumpOut;
		[Header("bump speed [TIME]")]
		[RangeAttribute(0f, 10f)]
		public float BumpSpeed;
		[Header("bump fade in and out [TIME]")]
		[RangeAttribute(0f, 10f)]
		public float FadeIn;
		[RangeAttribute(0f, 10f)]
		public float FadeOut;
		[Header("bump pause between in and out [TIME]")]
		[RangeAttribute(0f, 10f)]
		public float BumpPause;
		[RangeAttribute(0f, 1f)]
		[Header("fade in max alpha value [1 = 100%]")]
		public float DefaultAlpha;
		[System.NonSerialized]
		public Vector2 TargetIn;
		[System.NonSerialized]
		public Vector2 TargetOut;
		[System.NonSerialized]
		public bool ActiveFadeIn;
		[System.NonSerialized]
		public bool ActiveFadeOut;
		[System.NonSerialized]
		public bool ActiveBumpIn;
		[System.NonSerialized]
		public bool ActiveBumpOut;
		[System.NonSerialized]
		public bool DeleteMe;

		[Space(10)]
		public UnityEvent BumpInComplete;
		public UnityEvent BumpOutComplete;
		public bool Active;

		public clsBump (Vector2 bumpinoffset,
			Vector2 bumpoutoffset,
			bool triggerbumpout,
			GameObject sourcescene,
			GameObject sourceobject,
			float bumppause,
			float bumpspeed,
			float fadein,
			float fadeout,
			float defaultalpha,
			Vector2 targetin,
			Vector2 targetout,
			bool activefadein,
			bool activefadeout,
			bool activebumpin,
			bool activebumpout,
			bool active,
			Vector2 objsize,
			bool deleteme
		) {
			BumpInOffset = bumpinoffset;
			BumpOutOffset = bumpoutoffset;
			TriggerBumpOut = triggerbumpout;
			SourceScene = sourcescene;
			SourceObject = sourcescene;
			BumpSpeed = bumpspeed;
			BumpPause = bumppause;
			FadeIn = fadein;
			DefaultAlpha = defaultalpha;
			FadeOut = fadeout;
			TargetIn = targetin;
			TargetOut = targetout;
			ActiveFadeIn = activefadein;
			ActiveFadeOut = activefadeout;
			ActiveBumpIn = activebumpin;
			ActiveBumpOut = activebumpout;
			Active = active;
			ObjSize = objsize;
			DeleteMe = deleteme;
		}
		public clsBump () {

			BumpInOffset = Vector2.zero;
			BumpOutOffset = Vector2.zero;
			TriggerBumpOut = false;
			SourceScene = null;
			SourceObject = null;
			BumpSpeed = 1;
			BumpPause = 0;
			FadeIn = 0;
			DefaultAlpha = 1;
			FadeOut = 0;
			TargetIn = Vector2.zero;
			TargetOut = Vector2.zero;
			ActiveFadeIn = false;
			ActiveFadeOut = false;
			ActiveBumpIn = false;
			ActiveBumpOut = false;
			Active = false;
			ObjSize = Vector2.zero;
			DeleteMe = false;
		}
	}
	[Header("BUMP OBJECT IN AND OUT OF VIEW")]
	public clsBump bump;

	private float currentLerpTime;
	private Vector3 startPos;
	private CanvasGroup canvasgroup;
	private bool StoreCurrentPosition = false;
	private Vector3 CurrentPosition;
	private bool initialized = false;

	// user input variable backup
	//
	private GameObject SourceSceneSet;
	private GameObject SourceObjectSet;
	private Vector2 BumpInOffsetSet;
	private Vector2 BumpOutOffsetSet;
	private float BumpSpeedSet;
	private float FadeInSet;
	private float FadeOutSet;
	private float BumpPauseSet;
	private float DefaultAlphaSet;

	// v1.4 updated the following to FALSE
	//
	private bool NoLerpSet = false;
	private bool TriggerBumpOutSet = false;

	#endregion

	void OnEnable ()
	{
		StartCoroutine (Delay ());
	}

	void OnDisable () 
	{
		initialized = false;
	}

	IEnumerator Delay() 
	{
		if (!bump.Active) 
		{
			InitializeCanvas ();
		}
		yield return new WaitForEndOfFrame();
		if (!initialized) 
		{
			ResetVariables ();
			KickStart ();
			initialized = true;
		}
	}

	void InitializeCanvas ()
	{
		canvasgroup = GetComponent<CanvasGroup>();
		if (canvasgroup != null)
			canvasgroup.alpha = 0;
	}

	void ResetVariables () 
	{
		SourceSceneSet = bump.SourceScene;
		SourceObjectSet = bump.SourceObject;
		BumpInOffsetSet = bump.BumpInOffset;
		BumpOutOffsetSet = bump.BumpOutOffset;
		NoLerpSet = bump.NoLerp;
		TriggerBumpOutSet = bump.TriggerBumpOut;
		BumpSpeedSet = bump.BumpSpeed;
		FadeInSet = bump.FadeIn;
		FadeOutSet = bump.FadeOut;
		BumpPauseSet = bump.BumpPause;
		DefaultAlphaSet = bump.DefaultAlpha;

		bump.ActiveFadeOut = false;
		bump.ActiveFadeIn = false;
		bump.ActiveBumpIn = false;
		bump.ActiveBumpOut = false;
	}

	void KickStart ()
	{
		if (!StoreCurrentPosition) 
		{
			CurrentPosition = transform.localPosition;
			StoreCurrentPosition = true;
		}

		Vector2 rectsize = Vector2.zero;
		Vector2 startxy = CurrentPosition;
		Vector2 endxy = CurrentPosition;

		if (SourceObjectSet == null) 
		{
			bump.ObjSize = GetComponent<RectTransform>().rect.size * 0.5f;
		} else 
		{
			bump.ObjSize = SourceObjectSet.GetComponent<RectTransform>().rect.size * 0.5f;
		}

		if (SourceSceneSet != null)
		{
			if (BumpInOffsetSet.x == 0)
				bump.ObjSize.x = 0;
			
			if (BumpInOffsetSet.y == 0)
				bump.ObjSize.y = 0;

			//rectsize = (SourceSceneSet.GetComponent<RectTransform>().rect.size * 0.5f) + bump.ObjSize;
            rectsize = SourceSceneSet.GetComponent<RectTransform>().rect.size;

            if (BumpInOffsetSet.x > 0)
			{
				startxy.x = rectsize.x;
			} else if (BumpInOffsetSet.x < 0)
			{
				startxy.x = -(rectsize.x);
			}
			if (BumpInOffsetSet.y > 0)
			{
				startxy.y = rectsize.y;
			} else if (BumpInOffsetSet.y < 0)
			{
				startxy.y = -(rectsize.y);
			}
			if (BumpOutOffsetSet.x == 0)
				bump.ObjSize.x = 0;
			
			if (BumpOutOffsetSet.y == 0)
				bump.ObjSize.y = 0;

			//rectsize = (SourceSceneSet.GetComponent<RectTransform>().rect.size * 0.5f) + bump.ObjSize;
            rectsize = SourceSceneSet.GetComponent<RectTransform>().rect.size;

            if (BumpOutOffsetSet.x > 0)
			{
				endxy.x = rectsize.x;
			} else if (BumpOutOffsetSet.x < 0) 
			{
				endxy.x = -(rectsize.x);
			}
			if (BumpOutOffsetSet.y > 0) 
			{
				endxy.y = rectsize.y;
			} else if (BumpOutOffsetSet.y < 0)
			{
				endxy.y = -(rectsize.y);
			}

		} else 
		{
			startxy += BumpInOffsetSet;
			endxy += BumpOutOffsetSet;
		}

		bump.TargetIn = CurrentPosition;

		if (BumpInOffsetSet != Vector2.zero) 
		{
			bump.ActiveBumpIn = true;
			bump.TargetIn = CurrentPosition;
		}

		if (BumpOutOffsetSet != Vector2.zero)
		{
			if (!TriggerBumpOutSet)
				bump.ActiveBumpOut = true;
		}

		transform.localPosition = startxy;
		bump.TargetOut = endxy;

		if (canvasgroup != null)
		{
			if (FadeInSet > 0)
			{ 
				bump.ActiveFadeIn = true;
				canvasgroup.alpha = 0;
			} else
			{
				canvasgroup.alpha = DefaultAlphaSet;
			}
			if (FadeOutSet > 0)
			{
				if (!bump.ActiveFadeIn)
					canvasgroup.alpha = DefaultAlphaSet;
				
				if (!TriggerBumpOutSet)
					bump.ActiveFadeOut = true;
			}
		} else 
		{
			Debug.LogWarning ("Pop: Missing Canvas on " + this.name);
		}
			
		//
		//
		ConvertTimers ();

		//
		//
		ResetLerpVariables ();

		if (bump.ActiveFadeIn || bump.ActiveFadeOut || bump.ActiveBumpIn || bump.ActiveBumpOut)
			bump.Active = true;
	}

	void FixedUpdate () 
	{
		if (bump.Active)
			HandleBump ();
	}

	public void TriggerBumpOut () 
	{
		if (FadeOutSet > 0)
			bump.ActiveFadeOut = true;
		
		if (BumpOutOffsetSet != Vector2.zero)
			bump.ActiveBumpOut = true;
		
        BumpPauseSet = 0;
		bump.Active = true;
	}

	void HandleBump ()
	{
		bool bumpincomplete = false;
		bool bumpoutcomplete = false;

		// handle fading the object in / out
		//
		if (canvasgroup != null)
		{
			if (bump.ActiveFadeIn)
			{
				if (canvasgroup.alpha < DefaultAlphaSet) 
				{
					canvasgroup.alpha += FadeInSet;
				} else 
				{
					bumpincomplete = true;
					bump.ActiveFadeIn = false;
				}
			} else if (BumpPauseSet > 0) 
			{
				BumpPauseSet --;
			} else if (bump.ActiveFadeOut && !bump.ActiveBumpIn)
			{
				if (canvasgroup.alpha > 0)
				{
					canvasgroup.alpha -= FadeOutSet;
				} else 
				{
					bumpoutcomplete = true;
					bump.ActiveFadeOut = false;
				}
			}
		}

		// handle moving the object in / out
		//
		if (bump.ActiveBumpIn) 
		{
			if (Vector3.Distance (transform.localPosition, bump.TargetIn) > 1f) 
			{
				if (NoLerpSet)
				{
					transform.localPosition = Vector3.MoveTowards (transform.localPosition, bump.TargetIn, BumpSpeedSet);
				} else 
				{
					currentLerpTime += Time.deltaTime;
					if (currentLerpTime > BumpSpeedSet) 
					{
						currentLerpTime = BumpSpeedSet;
					}
					float n = currentLerpTime / BumpSpeedSet;
					// ease out
					//
					n = Mathf.Sin(n * Mathf.PI * 0.5f);
					// ease in
					//
					// n = 1f - Mathf.Cos(n * Mathf.PI * 0.5f);
					transform.localPosition = Vector3.Lerp(startPos, bump.TargetIn, n);
				}
			} else
			{
				ResetLerpVariables ();
				bump.ActiveBumpIn = false;
				bumpincomplete = true;
			}

		} else if (BumpPauseSet > 0) 
		{
			BumpPauseSet --;
		} else if (bump.ActiveBumpOut && !bump.ActiveFadeIn) 
		{
			if (Vector3.Distance (transform.localPosition, bump.TargetOut) > 1f)
			{
				if (NoLerpSet)
				{
					transform.localPosition = Vector3.MoveTowards (transform.localPosition, bump.TargetOut, BumpSpeedSet);
				} else
				{
					currentLerpTime += Time.deltaTime;
					if (currentLerpTime > BumpSpeedSet)
					{
						currentLerpTime = BumpSpeedSet;
					}
					float n = currentLerpTime / BumpSpeedSet;
					n = Mathf.Sin(n * Mathf.PI * 0.5f);
					transform.localPosition = Vector3.Lerp(startPos, bump.TargetOut, n);
				}
			} else
			{
				ResetLerpVariables ();
				bump.ActiveBumpOut = false;
				bumpoutcomplete = true;
			}
		}
		// fully moved / faded in / out checks
		//
		if (bumpincomplete && !bump.ActiveFadeIn && !bump.ActiveBumpIn) 
		{
			bump.BumpInComplete.Invoke();
		}
		if (bumpoutcomplete && !bump.ActiveFadeOut && !bump.ActiveBumpOut)
		{
			bump.BumpOutComplete.Invoke();
		}
		// end script check
		//
		if (!bump.ActiveFadeIn && !bump.ActiveFadeOut && !bump.ActiveBumpIn && !bump.ActiveBumpOut) 
		{
			bump.Active = false;
			initialized = false;
			if (bump.DeleteMe)
			{
				DeleteThisObject ();
			}
		}
	}

	void ResetLerpVariables ()
	{
		currentLerpTime = 0f;
		startPos = transform.localPosition;
	}

	void ConvertTimers ()
	{
		float currentfps = 1.0f / Time.fixedDeltaTime;

		if (BumpSpeedSet != 0 && NoLerpSet)
		{
			float n = Vector3.Distance (bump.TargetIn, transform.localPosition) / (BumpSpeedSet * currentfps);
			BumpSpeedSet = n;
		}
		if (FadeInSet != 0)
		{
			float frametotal = FadeInSet * currentfps;
			FadeInSet = 1f / frametotal;
		}
		if (FadeOutSet != 0)
		{
			float frametotal = FadeOutSet * currentfps;
			FadeOutSet = 1f / frametotal;
		}
		if (BumpPauseSet != 0)
		{
			BumpPauseSet *= currentfps;
		}
	}

	public void DeleteThisObject ()
	{
		Destroy (gameObject);
	}
}