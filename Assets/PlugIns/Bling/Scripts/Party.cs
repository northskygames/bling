﻿//
// V1.47
//
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.Events;

public class Party : MonoBehaviour
{
	#region VARIABLES

	[System.Serializable]
	public class cCreator
	{
		[System.NonSerialized]
		public int MaxParticlesCounter;
		public Transform AlternateParent;
		public bool useGrandParent;
		public bool autoMask;
		[Space(10)]
		public bool UseScreenLimits;
		public bool ignoreLimits;
		[Space(10)]
		public bool infiniteParticles;
		[RangeAttribute(1, 1000)]
		public int MaxParticles;
		[System.NonSerialized]
		public float PawzSet;
		[Space(10)]
		[Tooltip("Create a delay between particle spawns")]
		public float Pawz;
		public bool RandomPawz;
		[Space(10)]
		[Tooltip("Remove particles that fade out / go off-screen")]
		public bool Burst;
		public Vector2 PivotPoint;
		[Space(10)]
		public GameObject[] AlternateSprite = new GameObject[1];
		public Sprite[] SpriteImage = new Sprite[1];

		public cCreator(
			bool infiniteparticles,
			int maxparticlescounter,
			bool usegrandparent,
			Transform alternateparent,
			bool usescreenlimits,
			bool ignorelimits,
			int maxparticles,
			GameObject[] alternatesprite,
			Sprite[] spriteimage,
			Vector2 pivotpoint,
			float pawz,
			float pawzset,
			bool randompawz,
			bool burst
		)
		{
			infiniteParticles = infiniteparticles;
			MaxParticlesCounter = maxparticlescounter;
			useGrandParent = usegrandparent;
			UseScreenLimits = usescreenlimits;
			ignoreLimits = ignorelimits;
			AlternateParent = alternateparent;
			AlternateSprite = alternatesprite;
			MaxParticles = maxparticles;
			SpriteImage = spriteimage;
			PivotPoint = pivotpoint;
			Pawz = pawz;
			PawzSet = pawzset;
			RandomPawz = randompawz;
			Burst = burst;
		}

		public cCreator()
		{
			UseScreenLimits = false;
			AlternateParent = null;
			AlternateSprite = null;
			SpriteImage = null;
			infiniteParticles = false;
			MaxParticles = 1;
			PivotPoint = new Vector2(0.5f, 0.5f);
			Pawz = 0;
			PawzSet = 0;
			RandomPawz = false;
			Burst = false;
		}
	}

    [Header("PARTICLE VOLUME / TYPE / ORIGIN")]
	[Header("PARTY LIKE ITS 'V1.47'")]
	public cCreator Creator;

	[System.Serializable]
	public class cMover
	{
		[RangeAttribute(0, 1000f)]
		public float Speed;
		public bool RandomSpeed;
		[Tooltip("Add force to a particles movement (0) random")]
		public Vector2 Delta;
		public bool RandomDelta;
		[Tooltip("Restrict the movement to either X or Y never both")]
		public float clampDeltaTime;
		[Space(10)]
		public bool Bounce;
		[RangeAttribute(0, 5f)]
		public float Throttle;
		public bool RandomThrottle;
		public Vector2 Gravity;
		[RangeAttribute(-10, 10)]
		public float GravityMultiplier;
		public bool RandomGravity;
		[Space(10)]
		[Tooltip("Add rotation to a particles path, (0) off")]
		public Vector2 SwirlVelocity;
		public bool RandomVelocity;
		public bool RandomDirection;
		[Tooltip("Sync x / y")]
		public bool LockVelocity;
		[Tooltip("Distance from particles origin")]
		public Vector2 SwirlRadius;
		public bool RandomRadius;
		[Tooltip("Sync x / y")]
		public bool LockRadius;
		[System.NonSerialized]
		public Vector2[] clampDeltas = new Vector2[4];

		//
		//

		public cMover(bool bounce,
					   float speed,
		               bool randomspeed,
		               Vector2 delta,
		               bool randomdelta,
		               float throttle,
		               bool randomthrottle,
		               Vector2 gravity,
		               float gravitymultiplier,
		               bool randomgravity,
		               Vector2 swirlvelocity,
		               bool randomvelocity,
		               bool randomdirection,
		               bool lockvelocity,
		               Vector2 swirlradius,
		               bool randomradius,
					   float clampdeltatime,
		               bool lockradius)
		{
			Bounce = bounce;
			Speed = speed;
			RandomSpeed = randomspeed;
			Delta = delta;
			RandomDelta = randomdelta;
			Throttle = throttle;
			RandomThrottle = randomthrottle;
			Gravity = gravity;
			GravityMultiplier = gravitymultiplier;
			RandomGravity = randomgravity;
			SwirlVelocity = swirlvelocity;
			RandomVelocity = randomvelocity;
			RandomDirection = randomdirection;
			LockVelocity = lockvelocity;
			SwirlRadius = swirlradius;
			RandomRadius = randomradius;
			clampDeltaTime = clampdeltatime;
			LockRadius = lockradius;
		}

		public cMover()
		{
			Bounce = false;
			Speed = 0;
			RandomSpeed = false;
			Delta = Vector2.zero;
			RandomDelta = true;
			Throttle = 0;
			RandomThrottle = false;
			Gravity = Vector2.zero;
			GravityMultiplier = 1.001f;
			RandomGravity = false;
			SwirlVelocity = Vector2.zero;
			RandomVelocity = false;
			RandomDirection = false;
			LockVelocity = false;
			SwirlRadius = Vector2.zero;
			RandomRadius = false;
			clampDeltaTime = 0;
			LockRadius = false;
		}
	}

	[Header("PHYSICS / MOTION")]
	public cMover Mover;

	[System.Serializable]
	public class cSpawner
	{
		[Tooltip("Force particles to spawn within set regions")]
		[RangeAttribute(0, 1000)]
		public int TopLeft;
		[RangeAttribute(0, 1000)]
		public int TopRight;
		[RangeAttribute(0, 1000)]
		public int BottomLeft;
		[RangeAttribute(0, 1000)]
		public int BottomRight;
		[Space(10)]
		public bool ClampTop;
		public bool ClampBottom;
		public bool ClampLeft;
		public bool ClampRight;
		[Space(10)]
		[Tooltip("Have particles spawn within a defined area")]
		public Vector2 FxArea;
		[Tooltip("Force particles to spawn uniformly around the shape of a circle, FxArea affects the randomness of the particles around the circle")]
		public Vector2 RadialSpawn;
		[Tooltip("+ or - the maximum spawn area")]
		[RangeAttribute(-500, 500)]
		public int Border;
		public bool BorderRandom;

		[Header("OLD VALUES, COPY THESE TO 'BURST' 'PAWZ' (ABOVE)")]
		public bool Burst; // TBDELETED
		public float PopPop; // TBDELETED

		public cSpawner(int topleft,
		                 int topright,
		                 int bottomleft,
		                 int bottomright,
		                 bool clamptop,
		                 bool clampbottom,
		                 bool clampleft,
		                 bool clampright,
		                 Vector2 fxarea,
		                 int border,
						 bool borderrandom,

						 bool burst, // TBDELETED
						 float poppop // TBDELETED
		)
		{
			TopLeft = topleft;
			TopRight = topright;
			BottomLeft = bottomleft;
			BottomRight = bottomright;
			ClampTop = clamptop;
			ClampBottom = clampbottom;
			ClampLeft = clampleft;
			ClampRight = clampright;
			FxArea = fxarea;
			Border = border;
			BorderRandom = borderrandom;

			Burst = burst; // TBDELETED
			PopPop = poppop; // TBDELETED
		}

		public cSpawner()
		{
			TopLeft = 0;
			TopRight = 0;
			BottomLeft = 0;
			BottomRight = 0;
			ClampTop = false;
			ClampBottom = false;
			ClampLeft = false;
			ClampRight = false;
			FxArea = Vector2.zero;
			Border = 0;
			BorderRandom = false;

			Burst = false; // TBDELETED
			PopPop = 0; // TBDELETED
		}
	}

	[Header("SPAWN PATTERN")]
	public cSpawner Spawner;

	[System.Serializable]
	public class cShaper
	{
		[Tooltip("Resize over time (-1) max screen x / y")]
		public Vector2 SizeMod;
		[Tooltip("Set the start size of a particle that is going to be resized over time. Example; " +
            " SizeMod X = 128, Y = 64 ... SizeModStart X = 64, Y = 64 would result in the particle starting with a size of 64, 64 and growing to 128, 64" +
            " thus the Y would not change. This is handy when creating set sized 'beams'")]
		public Vector2 SizeModStart;
		public bool RandomSize;
		[Tooltip("Resize rate (-1) instant")]
		[RangeAttribute(-1, 1000)]
		public float SizeModRate;
		[Space(10)]
		[Tooltip("Assign random scale")]
		[RangeAttribute(0f, 10f)]
		public float ScaleMin;
		[RangeAttribute(0f, 10f)]
		public float ScaleMax;
		[Space(10)]
		public Vector3 Rotate;
		public Vector3 SetRotation;
		public bool RandomRotate;
		public bool RandomRotation;
		public bool RandomFlip;

		public cShaper(
			Vector2 sizemod,
			Vector2 sizemodstart,
			bool randomsize,
			float sizemodrate,
			float scalemin,
			float scalemax,
			Vector3 rotate,
			Vector3 setrotation,
			bool randomrotate,
			bool randomrotation,
			bool randomflip
		)
		{
			SizeMod = sizemod;
			SizeModStart = sizemodstart;
			RandomSize = randomsize;
			SizeModRate = sizemodrate;
			ScaleMin = scalemin;
			ScaleMax = scalemax;
			Rotate = rotate;
			SetRotation = setrotation;
			RandomRotate = randomrotate;
			RandomRotation = randomrotation;
			RandomFlip = randomflip;
		}

		public cShaper()
		{
			SizeMod = Vector2.zero;
			SizeModStart = Vector2.zero;
			RandomSize = false;
			SizeModRate = 0;
			ScaleMin = 0;
			ScaleMax = 0;
			Rotate = Vector3.zero;
			SetRotation = Vector3.zero;
			RandomRotate = false;
			RandomRotation = false;
			RandomFlip = false;
		}
	}

	[Header("MODIFY SCALE / SIZE")]
	public cShaper Shaper;

	[System.Serializable]
	public class cMixer
	{
		[System.NonSerialized]
		public float PulsarDelaySet;
		public Material FxMaterial;
		[Space(10)]
		public Color Colour;
		public bool Mixer;
		public bool LockRed;
		public bool	LockGreen;
		public bool LockBlue;
		public bool NoRed;
		public bool NoGreen;
		public bool NoBlue;
		[Space(10)]
		[RangeAttribute(0f, 1f)]
		public float Desaturate;
		public bool RandomDesaturate;
		[Space(10)]
		public bool RandomAlpha;
		[Space(10)]
		[RangeAttribute(0f, 100f)]
		[Tooltip("Colour pulse interval")]
		public float PulsarDelay;
		[Tooltip("Colours (0 + 1) use a particles colour by default")]
		public Color[] PulsarColour = new Color[2];

		public cMixer(
			float pulsardelayset,
			Material fxmaterial,
			Color colour,
			bool mixer,
			bool lockred,
			bool lockgreen,
			bool lockblue,
			bool nored,
			bool nogreen,
			bool noblue,
			float desaturate,
			bool randomdesaturate,
			bool randomalpha,
			float pulsardelay,
			Color[] pulsarcolour)
		{
			PulsarDelaySet = pulsardelayset;
			FxMaterial = fxmaterial;
			Colour = colour;
			Mixer = mixer;
			LockRed = lockred;
			LockGreen = lockgreen;
			LockBlue = lockblue;
			NoRed = nored;
			NoGreen = nogreen;
			NoBlue = noblue;
			Desaturate = desaturate;
			RandomDesaturate = randomdesaturate;
			RandomAlpha = randomalpha;
			PulsarDelay = pulsardelay;
			PulsarColour = pulsarcolour;
		}

		public cMixer()
		{
			PulsarDelaySet = 0;
			FxMaterial = null;
			Colour = Color.white;
			Mixer = false;
			LockRed = false;
			LockGreen = false;
			LockBlue = false;
			NoRed = false;
			NoGreen = false;
			NoBlue = false;
			Desaturate = 0;
			RandomDesaturate = false;
			RandomAlpha = false;
			PulsarDelay = 0;
		}
	}

	[Header("COLOUR EFFECTS")]
	public cMixer Mixer;

	[System.Serializable]
	public class cFader
	{
		[System.NonSerialized]
		public float FadeInSet;
		[System.NonSerialized]
		public float FadeOutSet;
		[System.NonSerialized]
		public float FadeDelaySet;
		[System.NonSerialized]
		public float LifeSet;

		[Tooltip("Numeric values")]
		[RangeAttribute(0f, 50f)]
		public float FadeIn;
		public bool RandomFadeIn;
		[RangeAttribute(0f, 500f)]
		public float FadeDelay;
		public bool RandomFadeDelay;
		[RangeAttribute(0f, 50f)]
		public float FadeOut;
		public bool RandomFadeOut;
		[Space(10)]
		[RangeAttribute(0, 1000)]
		public float Life;
		public bool RandomLife;

		public cFader(
			float fadeinset,
			float fadeoutset,
			float fadedelayset,
			float lifeset,
			float fadein,
			bool randomfadein,
			float fadedelay,
			bool randomfadedelay,
			float fadeout,
			bool randomfadeout,
			float life,
			bool randomlife)
		{
			FadeInSet = fadeinset;
			FadeOutSet = fadeoutset;
			FadeDelaySet = fadedelayset;
			LifeSet = lifeset;
			FadeIn = fadein;
			RandomFadeIn = randomfadein;
			FadeDelay = fadedelay;
			RandomFadeDelay = randomfadedelay;
			FadeOut = fadeout;
			RandomFadeOut = randomfadeout;
			Life = life;
			RandomLife = randomlife;
		}

		public cFader()
		{
			FadeInSet = 0;
			FadeOutSet = 0;
			FadeDelaySet = 0;
			LifeSet = 0;
			FadeIn = 0;
			RandomFadeIn = false;
			FadeDelay = 0;
			RandomFadeDelay = false;
			FadeOut = 0;
			RandomFadeOut = false;
			Life = 0;
			RandomLife = false;
		}
	}

	[Header("ALPHA / PARTICLE LIFE")]
	public cFader Fader;

	[System.Serializable]
	public class cParticle
	{
		public GameObject Obj;
		public RectTransform RecT;
		public Image Img;
		public float TrailDelay;
		public bool Active;
		public bool Bounce;
		public float Speed;
		public Vector3 Delta;
		public float Throttle;
		public Vector3 Gravity;
		public Vector3 SizeMod;
		public float SizeRateMath;
		public float ScaleMin;
		public float ScaleMax;
		public Vector3 Rotate;
		public int OriginX;
		public int OriginY;
		public Vector2 SwirlVelocity;
		public Vector2 SwirlVelocitySet;
		public Vector2 SwirlRadius;
		public Color Colour;
		public Color ColourSet;
		public float Desaturate;
		public float PulsarDelay;
		public Color[] PulsarColour;
		public int PulsarColourPick;
		public float FadeIn;
		public float FadeDelay;
		public float FadeOut;
        public float FadeInAlpha;
		public float Life;
        public CanvasGroup MyCanvas;
		//
		//
		//
		public float clampDeltaTime;
		public int clampIndex;
		public int clampFlipFlop;

		public cParticle(GameObject obj,
                          RectTransform trect,
    	                  Image img,
    	                  float traildelay,
    	                  bool active,
						  bool bounce,
    	                  float speed,
    	                  Vector3 delta,
    	                  float throttle,
    	                  Vector3 gravity,
    	                  Vector3 sizemod,
    	                  float sizeratemath,
    	                  float scalemin,		                   
    	                  float scalemax,
    	                  Vector3 rotate,
    	                  int originx,
    	                  int originy,
    	                  Vector2 swirlvelocity,
    	                  Vector2 swirlvelocityset,
    	                  Vector2 swirlradius,
    	                  Color colour,
    	                  Color colourset,
    	                  float desaturate,
    	                  float pulsardelay,
    	                  Color[] pulsarcolour,
    	                  int pulsarcolourpick,
    	                  float fadein,
    	                  float fadedelay,
    	                  float fadeout,
                          float fadeinalpha,
    	                  float life,
						  float clampdeltatime,
						  int clampindex,
						  int clampflipflop,
						  CanvasGroup mycanvas)
		{

			Obj = obj;
			RecT = trect;
			Img = img;
			TrailDelay = traildelay;
			Active = active;
			Bounce = bounce;
			Speed = speed;
			Delta = delta;
			Throttle = throttle;
			Gravity = gravity;
			SizeMod = sizemod;
			SizeRateMath = sizeratemath;
			ScaleMin = scalemin;
			ScaleMax = scalemax;
			Rotate = rotate;
			OriginX = originx;
			OriginY = originy;
			SwirlVelocity = swirlvelocity;
			SwirlVelocitySet = swirlvelocityset;
			SwirlRadius = swirlradius;
			Colour = colour;
			ColourSet = colourset;
			Desaturate = desaturate;
			PulsarDelay = pulsardelay;
			PulsarColour = pulsarcolour;
			PulsarColourPick = pulsarcolourpick;
			FadeIn = fadein;
			FadeDelay = fadedelay;
			FadeOut = fadeout;
            FadeInAlpha = fadeinalpha;
			Life = life;
            MyCanvas = mycanvas;
			clampDeltaTime = clampdeltatime;
			clampIndex = clampindex;
			clampFlipFlop = clampflipflop;
		}

		public cParticle()
		{
			Obj = null;
			RecT = null;
			Img = null;
			TrailDelay = 0;
			Active = false;
			Bounce = false;
			Speed = 0;
			Delta = Vector3.zero;
			Throttle = 0;
			Gravity = Vector3.zero;
			SizeMod = Vector3.zero;
			SizeRateMath = 0;
			ScaleMin = 0;
			ScaleMax = 0;
			Rotate = Vector3.zero;
			OriginX = 0;
			OriginY = 0;
			SwirlVelocity = Vector2.zero;
			SwirlVelocitySet = Vector2.zero;
			SwirlRadius = Vector2.zero;
			Colour = Color.white;
			ColourSet = Color.white;
			Desaturate = 0;
			PulsarDelay = 0;
			PulsarColour = null;
			PulsarColourPick = 0;
			FadeIn = 0;
			FadeDelay = 0;
			FadeOut = 0;
            FadeInAlpha = 0;
			Life = 0;
            MyCanvas = null;
			clampDeltaTime = 0;
			clampIndex = 0;
			clampFlipFlop = 0;
		}
	}

	[Space(10)]
	public UnityEvent partyOver;

	[System.NonSerialized]
	public Vector3 canvasScale;

	private List <cParticle> party = new List<cParticle>();
	private Transform Parent;
	private Vector2[] Orb;
	private Canvas canvas;
	private Vector2 screenSizeSet;
	private Vector2 parentSize;
	private Vector2 screenSize;
	//
	//
	private float radialIncrement;
	private float radialIndex;
	//
    //
    private bool[] PulsarColourSet = new bool[] { false, false };
	private float CurrentFPS;
	public bool PartyDown = false;
	private bool initialized = false;
	private bool firstRun = true;

	const float rndhi = 1.5f;
	const float rndlo = 0.5f;

	public static bool isPartyEnabled = true;

	#endregion

	// 
	// OnEnable
	//

	void OnEnable()
	{
		if (!isPartyEnabled)
		{
			if (partyOver != null)
				partyOver.Invoke();
		}
		else
		{
			StartCoroutine(Delay());
		}
	}

	//
	// OnDisable
	//

	void OnDisable()
	{
		if (isPartyEnabled)
        {
			ResetParty();
			StopAllCoroutines();
			initialized = false;
		}
	}

	//
	// ResetParty
	//

	void ResetParty()
	{
		foreach (cParticle node in party)
		{
			if (node.Obj != null)
            {
				Destroy(node.Obj);
			}
		}
		party.Clear();
	}

	//
	// ShutDown
	//

	void ShutDown()
    {
		Debug.LogWarning(@"Party: SHUTDOWN");
		PartyDown = false;
		enabled = false;
    }

	//
	// StopPawz
	//

	public void StopPawz()
    {
		Creator.Pawz = 0;
    }

	//
	// Delay
	//

	IEnumerator Delay()
	{
		yield return new WaitForEndOfFrame();

		if (!isPartyEnabled)
			yield break;

		if (firstRun)
		{
			firstRun = false;

			Creator.PawzSet = Creator.Pawz;
			Creator.MaxParticlesCounter = Creator.MaxParticles;
			Fader.FadeInSet = Fader.FadeIn;
			Fader.FadeOutSet = Fader.FadeOut;
			Fader.FadeDelaySet = Fader.FadeDelay;
			Fader.LifeSet = Fader.Life;
			Mixer.PulsarDelaySet = Mixer.PulsarDelay;
		}

		if (!initialized)
		{
			if (KickStart())
			{
				initialized = true;
			}
			else
			{
				ShutDown();
				yield break;
			}
		}

		//
		// Creator.Pawz
		//

		if (Creator.Pawz > 0)
		{
			float initialdelay = Creator.Pawz;

			if (Creator.RandomPawz)
			{
				initialdelay = Random.Range(0, Creator.Pawz);
			}

			StartCoroutine(Pawz(initialdelay));
		}
		else
		{
			for (int i = 0; i < Creator.MaxParticles; i++)
            {
                cParticle node = InitializeParticle();
                if (node != null)
                {
                    party.Add(node);
                }
                else
                {
                    ShutDown();
                    yield break;
                }
            }
		}

		PartyDown = true;
	}

	//
	// Pawz
	//

	IEnumerator Pawz(float delay)
	{
		while (Creator.Pawz != 0)
		{
			yield return new WaitForSeconds(delay);

			if (Creator.RandomPawz)
            {
				delay = Random.Range(0, Creator.Pawz);
			}

			if (Creator.infiniteParticles)
			{
				cParticle node = InitializeParticle();
				if (node != null)
				{
					party.Add(node);
				}
				else
				{
					ShutDown();
					yield break;
				}
			}
			else if (Creator.MaxParticlesCounter > 0)
			{
				Creator.MaxParticlesCounter--;

				cParticle node = InitializeParticle();
				if (node != null)
				{
					party.Add(node);
				}
				else
				{
					ShutDown();
					yield break;
				}
			}
			else
			{
				StopPawz();
			}
		}
	}

	//
	// KickStart
	//

	bool KickStart()
	{
		if (!isPartyEnabled)
			return false;

		//
		// ENFORCE FRAMERATE: Application.targetFrameRate = 30
		//

		//
		Application.targetFrameRate = 30;
		//

		CurrentFPS = 1.0f / Time.fixedDeltaTime;

		if (Creator.SpriteImage.Length < 1 && Creator.AlternateSprite.Length < 1)
		{
			Debug.LogWarning(@"Party: Both Sprite Images and Alternate Objects are EMPTY");
			return false;
		}

		//
		// Force this object > same size as PARENT (Has to happen BEFORE RefreshViewSizeVariables)
		//
		if (Creator.autoMask)
		{
			RectTransform thisRectTransform = GetComponent<RectTransform>();

			thisRectTransform.anchorMax = new Vector2(1, 1);
			thisRectTransform.anchorMin = new Vector2(0, 0);
			thisRectTransform.offsetMax = new Vector2(0, 0);
			thisRectTransform.offsetMin = new Vector2(0, 0);
		}
		//
		//
		//

		if (!RefreshViewSizeVariables())
		{
			Debug.LogWarning(@"Party: RefreshViewSizeVariables, ERROR!");
			return false;
		}

		// correct scale input errors
		if (Shaper.ScaleMin == 0 && Shaper.ScaleMax == 0)
		{
			Shaper.ScaleMin = 1;
			Shaper.ScaleMax = 1;
		}
		else if (Shaper.ScaleMax < Shaper.ScaleMin)
		{
			Shaper.ScaleMax = Shaper.ScaleMin;
		}

		// assign the parent to hold the particles
		if (Creator.AlternateParent != null)
		{
			Parent = Creator.AlternateParent;
		}
		else if (Creator.useGrandParent)
		{
			Parent = transform.parent.parent;
		}
		else
		{
			Parent = transform;
		}

		// pulsar setup
		if (Mixer.PulsarColour.Length < 2)
			Mixer.PulsarColour = new Color[2];

		if (Mixer.PulsarColour[0] != Color.clear)
			PulsarColourSet[0] = true;

		if (Mixer.PulsarColour[1] != Color.clear)
			PulsarColourSet[1] = true;

		Creator.Pawz = Creator.PawzSet;
		Creator.MaxParticlesCounter = Creator.MaxParticles;
		Fader.FadeIn = Fader.FadeInSet;
		Fader.FadeOut = Fader.FadeOutSet;
		Fader.FadeDelay = Fader.FadeDelaySet;
		Fader.Life = Fader.LifeSet;
		Mixer.PulsarDelay = Mixer.PulsarDelaySet;

		// clamp
		Mover.clampDeltas[0] = new Vector2(1, 0);
		Mover.clampDeltas[1] = new Vector2(0, -1);
		Mover.clampDeltas[2] = new Vector2(-1, 0);
		Mover.clampDeltas[3] = new Vector2(0, 1);

		// convert timers
		Fader.Life *= CurrentFPS;
		Fader.FadeDelay *= CurrentFPS;
		Mixer.PulsarDelay *= CurrentFPS;
		Mover.clampDeltaTime *= CurrentFPS;

		if (Fader.FadeIn != 0)
		{
			float frametotal = Fader.FadeIn * CurrentFPS;
			Fader.FadeIn = 1f / frametotal;
		}

		if (Fader.FadeOut != 0)
		{
			float frametotal = Fader.FadeOut * CurrentFPS;
			Fader.FadeOut = 1f / frametotal;
		}

		//
		//
		//
		if (Creator.autoMask)
		{
			if (!GetComponent<Image>())
				gameObject.AddComponent<Image>();

			if (!GetComponent<Mask>())
				gameObject.AddComponent<Mask>();

			GetComponent<Mask>().showMaskGraphic = false;

			Image parentImage = Parent.parent.GetComponent<Image>();

			if (!parentImage)
			{
				Debug.LogWarning(@"Party: autoMask, MISSING PARENT IMAGE");
				return false;
			}

			GetComponent<Image>().sprite = parentImage.sprite;
		}
		//
		//
		//

		// precalcs
		PreCalc(360);

		// clear the party invite list
		ResetParty();

		//
		//
		//
		radialIncrement = (float)(Orb.Length) / Creator.MaxParticles;
		radialIndex = 0;

		return true;
	}

	//
	// InitializeParticle
	//

	cParticle InitializeParticle()
    {
        cParticle newnode = new cParticle();
        GameObject obj = null;

        if (Creator.AlternateSprite.Length > 0)
        {
            GameObject alternate = Creator.AlternateSprite[Random.Range(0, Creator.AlternateSprite.Length)];

            if (alternate != null)
            {
                obj = Instantiate(alternate, Vector2.zero, Quaternion.identity) as GameObject;
                newnode.Img = obj.GetComponent<Image>();

				if (newnode.Img == null)
				{
					Debug.LogWarning(@"Party: Alternate sprite; MISSING IMAGE");
					return null;
				}

				newnode.Img.material = Mixer.FxMaterial;
                newnode.MyCanvas = obj.GetComponent<CanvasGroup>();
            }
            else
            {
				Debug.LogWarning(@"Party: Alternate sprite = NULL");
				return null;
			}
		}

        if (obj == null)
        {
            obj = new GameObject("particle");
            obj.AddComponent <Image>();
            newnode.Img = obj.GetComponent<Image>();
            newnode.Img.SetNativeSize();
            newnode.Img.material = Mixer.FxMaterial;
        }

		newnode.Obj = obj;
        newnode.Obj.SetActive(true);
        newnode.Obj.transform.SetParent(Parent, true);
        newnode.RecT = obj.GetComponent<RectTransform>();
        newnode.RecT.pivot = Creator.PivotPoint;
        newnode.Obj.transform.localScale = new Vector3(1, 1, 1);

		return InitializeParticle(newnode);
    }

	//
	// InitializeParticle
	//

	cParticle InitializeParticle(cParticle newnode)
	{
        if (Creator.AlternateSprite.Length == 0)
        {
            newnode.Img.sprite = Creator.SpriteImage[Random.Range(0, Creator.SpriteImage.Length)];
        }
        else
        {
            // need to randomize the object with a new object from the alternate object list
        }

        newnode.Active = true;

        // scale - reset size of object 1st
        newnode.Img.SetNativeSize();

		// scale affect object size or actual scale-size
		if (Creator.AlternateSprite.Length > 0)
			newnode.RecT.localScale = (new Vector3(1, 1, 1) * Random.Range(Shaper.ScaleMin, Shaper.ScaleMax));
		else
			newnode.RecT.sizeDelta *= Random.Range(Shaper.ScaleMin, Shaper.ScaleMax);

		// resize over time
		if (Shaper.SizeModRate != 0)
		{
			newnode.Img.type = Image.Type.Sliced;

			bool RandomSizeX = false;
			bool RandomSizeY = false;

			newnode.SizeMod = Shaper.SizeMod;
			newnode.SizeRateMath = 0;

			if (Shaper.RandomSize)
			{
				RandomSizeX = true;
				RandomSizeY = true;
			}

			// if -1 max size x
			if (newnode.SizeMod.x < 0)
			{
				newnode.SizeMod.x = Mathf.Abs(parentSize.x);
			}
			else if (RandomSizeX)
			{
				newnode.SizeMod.x = Random.Range(newnode.SizeMod.x * rndlo, newnode.SizeMod.x * rndhi);
			}
			
            // if -1 max size y
			if (newnode.SizeMod.y < 0)
			{
				newnode.SizeMod.y = Mathf.Abs(parentSize.y);
			}
			else if (RandomSizeY)
			{
				newnode.SizeMod.y = Random.Range(newnode.SizeMod.y * rndlo, newnode.SizeMod.y * rndhi);
			}

			//
			if (Shaper.SizeModRate == -1)
				newnode.RecT.sizeDelta = new Vector3(newnode.SizeMod.x, newnode.SizeMod.y, 0);
			else
				newnode.RecT.sizeDelta = Vector3.zero;
		}

		// life
		if (Fader.RandomLife)
			newnode.Life = Random.Range(Fader.Life * rndlo, Fader.Life * rndhi);
		else
			newnode.Life = Fader.Life;

		// reset colour
		newnode.ColourSet = Mixer.Colour;

		// alpha
        if (Mixer.RandomAlpha) {
            newnode.ColourSet.a = Random.Range(1f, 0f);
            newnode.FadeInAlpha = newnode.ColourSet.a;

        } else {
            newnode.FadeInAlpha = 1;
        }

		// colours
		if (Mixer.NoRed)
			newnode.ColourSet.r = 0f;

        if (Mixer.NoGreen)
			newnode.ColourSet.g = 0f;

        if (Mixer.NoBlue)
			newnode.ColourSet.b = 0f;

		if (Mixer.Mixer)
		{
			if (!Mixer.LockRed && !Mixer.NoRed)
				newnode.ColourSet.r = Random.Range(0f, 1f);

            if (!Mixer.LockGreen && !Mixer.NoGreen)
				newnode.ColourSet.g = Random.Range(0f, 1f);

            if (!Mixer.LockBlue && !Mixer.NoBlue)
				newnode.ColourSet.b = Random.Range(0f, 1f);
		}

		// desaturate
		if (Mixer.Desaturate > 0f)
		{
			Color decolor = newnode.ColourSet;
			float n = 0.3f * newnode.ColourSet.r + 0.6f * newnode.ColourSet.g + 0.1f * newnode.ColourSet.b;

            if (Mixer.RandomDesaturate)
				newnode.Desaturate = Random.Range(Mixer.Desaturate * rndlo, Mixer.Desaturate * rndhi);
			else
				newnode.Desaturate = Mixer.Desaturate;

            newnode.ColourSet = new Color(decolor.r + newnode.Desaturate * (n - decolor.r),
				decolor.g + newnode.Desaturate * (n - decolor.g),
				decolor.b + newnode.Desaturate * (n - decolor.b), 
				newnode.ColourSet.a);
		}

		// image
		newnode.Img.color = new Color(newnode.ColourSet.r, newnode.ColourSet.g, newnode.ColourSet.b, 0);

		// fades
		if (Fader.RandomFadeIn)
			newnode.FadeIn = Random.Range(Fader.FadeIn * rndlo, Fader.FadeIn * rndhi);
		else
			newnode.FadeIn = Fader.FadeIn;

        if (Fader.RandomFadeDelay)
			newnode.FadeDelay = Random.Range(Fader.FadeDelay * rndlo, Fader.FadeDelay * rndhi);
		else
			newnode.FadeDelay = Fader.FadeDelay;

        if (Fader.RandomFadeOut)
			newnode.FadeOut = Random.Range(Fader.FadeOut * rndlo, Fader.FadeOut * rndhi);
		else
			newnode.FadeOut = Fader.FadeOut;

		newnode.Colour = newnode.ColourSet;

		if (newnode.FadeIn != 0)
			newnode.Colour.a = 0;

		// pulsar
		if (!PulsarColourSet[0])
			Mixer.PulsarColour[0] = newnode.ColourSet;

        if (!PulsarColourSet[1])
			Mixer.PulsarColour[1] = newnode.ColourSet * 0.25f;

		newnode.PulsarColour = new Color[Mixer.PulsarColour.Length];

		for (int n = 0; n < newnode.PulsarColour.Length; n++)
			newnode.PulsarColour[n] = Mixer.PulsarColour[n];

		newnode.PulsarDelay = Mixer.PulsarDelay;
		newnode.PulsarColourPick = 0;

        // bounce
        newnode.Bounce = Mover.Bounce;

		// speed
		if (Mover.RandomSpeed)
			newnode.Speed = Random.Range(Mover.Speed * rndlo, Mover.Speed * rndhi);
		else
			newnode.Speed = Mover.Speed;

		// clamp
		newnode.clampIndex = Random.Range(0, Mover.clampDeltas.Length);
		newnode.clampDeltaTime = 0;
		newnode.clampFlipFlop = FlipFlop();

		// direction
		if (Mover.Delta.x == 0)
		{
			if (Mover.RandomDelta)
				newnode.Delta.x = Random.Range(-1f, 1f);
			else
				newnode.Delta.x = 0;
		}
		else
		{
			if (Mover.RandomDelta)
				newnode.Delta.x = FlipFlop() * (Random.Range(Mover.Delta.x * rndlo, Mover.Delta.x * rndhi));
			else
				newnode.Delta.x = Mover.Delta.x;
		}

        if (Mover.Delta.y == 0)
		{
			if (Mover.RandomDelta)
				newnode.Delta.y = Random.Range(-1f, 1f);
			else
				newnode.Delta.y = 0;
		}
		else
		{
			if (Mover.RandomDelta)
				newnode.Delta.y = FlipFlop() * (Random.Range(Mover.Delta.y * rndlo, Mover.Delta.y * rndhi));
			else
				newnode.Delta.y = Mover.Delta.y;
		}

		// gravity
		if (Mover.RandomGravity)
			newnode.Gravity = new Vector3(Random.Range(-Mover.Gravity.x, Mover.Gravity.x), Random.Range(Mover.Gravity.y * rndlo, Mover.Gravity.y * rndhi), 0);
		else
			newnode.Gravity = Mover.Gravity;

		// throttle
		if (Mover.RandomThrottle)
			newnode.Throttle = Random.Range(0f, Mover.Throttle * rndhi);
		else
			newnode.Throttle = Mover.Throttle;

		// swirl
		if (Mover.SwirlVelocity != Vector2.zero)
		{
			// swirl velocity
			if (Mover.RandomVelocity)
				newnode.SwirlVelocitySet = new Vector2(Random.Range(1, Mover.SwirlVelocity.x), Random.Range(1, Mover.SwirlVelocity.y));
			else
				newnode.SwirlVelocitySet = Mover.SwirlVelocity;

            if (Mover.RandomDirection)
				newnode.SwirlVelocitySet = new Vector2(newnode.SwirlVelocitySet.x * FlipFlop(), newnode.SwirlVelocitySet.y * FlipFlop());

            if (Mover.LockVelocity)
				newnode.SwirlVelocitySet.y = newnode.SwirlVelocitySet.x;

			int rv = Random.Range(0, Orb.Length);
			newnode.SwirlVelocity = new Vector2(rv, rv);

			// swirl radius
			if (Mover.RandomRadius)
				newnode.SwirlRadius = new Vector2(Random.Range(Mover.SwirlRadius.x * rndlo, Mover.SwirlRadius.x * rndhi), Random.Range(Mover.SwirlRadius.y * rndlo, Mover.SwirlRadius.y * rndhi));
			else
				newnode.SwirlRadius = Mover.SwirlRadius;

            if (Mover.LockRadius)
				newnode.SwirlRadius.y = newnode.SwirlRadius.x;

            newnode.SwirlRadius *= 0.5f;

			// swirl origin
			newnode.OriginX = (int)(newnode.Obj.transform.position.x + newnode.SwirlRadius.x);
			newnode.OriginY = (int)(newnode.Obj.transform.position.y + newnode.SwirlRadius.y);
		}

		// rotate
        newnode.Obj.transform.localRotation = new Quaternion (0,0,0,0);

        if (Shaper.RandomRotate)
			newnode.Rotate = new Vector3(Random.Range(Shaper.Rotate.x * rndlo, Shaper.Rotate.x * rndhi), Random.Range(Shaper.Rotate.y * rndlo, Shaper.Rotate.y * rndhi), Random.Range(Shaper.Rotate.z * rndlo, Shaper.Rotate.z * rndhi));
		else
			newnode.Rotate = Shaper.Rotate;

		// flip x
		if (Shaper.RandomFlip)
		{
            Vector3 localscale = newnode.Obj.transform.localScale;
            localscale.x *= FlipFlop();
            newnode.Obj.transform.localScale = localscale;
            newnode.Rotate *= FlipFlop();
        }
			
		// start rotation random or pre-set
		if (Shaper.RandomRotation)
			newnode.Obj.transform.Rotate(0, 0, Random.Range(0, 360));
		else
			newnode.Obj.transform.Rotate(Shaper.SetRotation);

		// portrait / landscape orientation change check
		if (screenSizeSet.x != canvas.GetComponent<RectTransform>().rect.width)
        {
			if (!RefreshViewSizeVariables())
			{
				ShutDown();
				return null;
			}
		}

		// finally, place the new particle
		PositionParticle(newnode);

		return newnode;
	}

	//
	//
	//

	private List<cParticle> partyDeleteList = new List<cParticle>();

	//
	// FixedUpdate
	//

	void FixedUpdate()
	{
		int partyAddCount = 0;

		if (PartyDown)
		{
            foreach (cParticle node in party)
			{
				Vector3 NewLoc = Vector3.zero;

				// swirl
				if (Mover.SwirlVelocity != Vector2.zero && Orb.Length > 0)
				{
					node.SwirlVelocity += new Vector2(node.SwirlVelocitySet.x, node.SwirlVelocitySet.y);

					if (node.SwirlVelocity.x >= Orb.Length)
						node.SwirlVelocity.x %= Orb.Length;

					while (node.SwirlVelocity.x < 0)
						node.SwirlVelocity.x += Orb.Length;

					if (node.SwirlVelocity.y >= Orb.Length)
						node.SwirlVelocity.y %= Orb.Length;

					while (node.SwirlVelocity.y < 0)
						node.SwirlVelocity.y += Orb.Length;

					if (node.SwirlVelocity.x >= 0 && node.SwirlVelocity.x < Orb.Length && node.SwirlVelocity.y >= 0 && node.SwirlVelocity.y < Orb.Length)
						NewLoc = new Vector3(Orb[(int)node.SwirlVelocity.x].x * node.SwirlRadius.x, Orb[(int)node.SwirlVelocity.y].y * node.SwirlRadius.y, 0);
				}

				// throttle
				if (node.Speed > node.Throttle)
					node.Speed -= node.Throttle;

				// gravity
				node.Gravity *= Mover.GravityMultiplier;

				// XYPathTime
				if (Mover.clampDeltaTime != 0)
                {
					node.clampDeltaTime--;

					if (node.clampDeltaTime <= 0)
                    {
						if (Mover.RandomDelta)
                        {
							node.clampDeltaTime = Random.Range(Mover.clampDeltaTime * 0.5f, Mover.clampDeltaTime * 1.5f);
							node.clampIndex = Random.Range(0, Mover.clampDeltas.Length);
						}
                        else
                        {
							node.clampDeltaTime = Mover.clampDeltaTime;
							node.clampIndex += node.clampFlipFlop;

							if (node.clampIndex > Mover.clampDeltas.Length-1)
                            {
								node.clampIndex = 0;
							}
							else if (node.clampIndex < 0)
							{
								node.clampIndex = Mover.clampDeltas.Length-1;
							}
						}

						node.Delta = new Vector3(Mover.clampDeltas[node.clampIndex].x * Mover.Delta.x, Mover.clampDeltas[node.clampIndex].y * Mover.Delta.y);
					}
				}

                //GameObject overrides == operator. It will return null if the object is being destroyed.
                if (node.Obj == null)
                {
                    partyDeleteList.Add(node);
                }
                else
                {
                    // movement
                    NewLoc += new Vector3(node.Speed * node.Delta.x, node.Speed * node.Delta.y, 0);

                    // update position
                    node.Obj.transform.localPosition += (NewLoc + node.Gravity) * Time.fixedDeltaTime;

                    // rotate object
                    node.Obj.transform.Rotate(node.Rotate.x * Time.fixedDeltaTime, node.Rotate.y * Time.fixedDeltaTime, node.Rotate.z * Time.fixedDeltaTime);

                    // size
                    if (Shaper.SizeModRate > 0)
                        ResizeSprite(node);

                    // offscreen check
                    if (!Creator.ignoreLimits)
                        OffViewCheck(node);

                    // reset and remove checks
                    HandleFades(node);

                    if (!node.Active)
                    {
                        partyDeleteList.Add(node);

                        if (!Creator.Burst)
                            partyAddCount++;
                    }
                    else
                    {
                        if (Mixer.PulsarDelay > 0)
                        {
                            node.PulsarDelay--;

                            if (node.PulsarDelay < 1)
                            {
                                node.PulsarDelay = Mixer.PulsarDelay;
                                node.PulsarColourPick = Random.Range(0, node.PulsarColour.Length);
                            }
                        }

                        // cycle the sprites colours
                        if (node.PulsarColourPick >= node.PulsarColour.Length)
                            node.PulsarColourPick %= node.PulsarColour.Length;

                        while (node.PulsarColourPick < 0)
                            node.PulsarColourPick += node.PulsarColour.Length;

                        if (node.PulsarColourPick >= 0 && node.PulsarColourPick < node.PulsarColour.Length)
                            node.Img.color = new Color(node.PulsarColour[node.PulsarColourPick].r, node.PulsarColour[node.PulsarColourPick].g, node.PulsarColour[node.PulsarColourPick].b, node.Colour.a);
                    }
                }
			}

            for (int i = 0; i < partyAddCount; i++)
            {
                if (partyDeleteList.Count > 0)
                {
                    cParticle newNode = partyDeleteList[0];

                    if (newNode.Obj == null)
                    {
                        party.Add(InitializeParticle());
                    }
                    else
                    {
                        InitializeParticle(newNode);
                        partyDeleteList.Remove(newNode);
                    }
                }
                else
                {
                    party.Add(InitializeParticle());
                }
            }

            foreach (cParticle node in partyDeleteList)
			{
				party.Remove(node);
				Destroy(node.Obj);
			}

			partyDeleteList.Clear();

			//
			// TRIGGER PARTY END CALL BACK WHEN PARTICLES = 0 HOWEVER IF POPPOP ACTIVE SKIP THIS CHECK
			//

			if (party.Count < 1 && Creator.Pawz == 0)
            {
				PartyDown = false;

				if (partyOver != null)
					partyOver.Invoke();
            }
		}
	}

	//
	// HandleFades
	//

	void HandleFades(cParticle node)
	{
		if (node.FadeIn > 0)
		{
			node.Colour.a += node.FadeIn;
            if (node.Colour.a > node.FadeInAlpha)
				node.FadeIn = 0;
		}
		else if (node.FadeDelay > 0)
		{
			node.FadeDelay -= 1;
		}
		else if (node.FadeOut > 0)
		{
			node.Colour.a -= node.FadeOut;
			if (node.Colour.a < 0)
				node.Active = false;
		}
		else if (node.Life > 0)
		{
			node.Life -= 1;
			if (node.Life < 1)
				node.Active = false;
		}

        if (node.MyCanvas == true)
            node.MyCanvas.alpha = node.Colour.a;
    }

	//
	// PreCalc
	//

	void PreCalc(int points)
	{
		Orb = new Vector2[points];

		double radius = 30;
		Vector2 center = new Vector2(0, 0);
		double slice = 2 * Mathf.PI / Orb.Length;

		for (int i = 0; i < Orb.Length; i++)
		{
			double angle = slice * i;
			Orb[i] = new Vector2((int)(center.x + radius * Mathf.Cos((float)angle)), (int)(center.y + radius * Mathf.Sin((float)angle)));
		}
	}

	//
	// ResizeSprite
	//

	void ResizeSprite(cParticle node)
	{
		node.SizeRateMath += Time.deltaTime;

		if (node.SizeRateMath > Shaper.SizeModRate)
			node.SizeRateMath = Shaper.SizeModRate;

		node.RecT.sizeDelta = Vector3.Lerp(Shaper.SizeModStart, node.SizeMod, node.SizeRateMath / Shaper.SizeModRate);
	}

	//
	// FlipFlop
	//

	int FlipFlop()
	{
		int n = Random.Range(0, 2) * 2 - 1;
		return n;
	}

	//
	// (GET SCREEN AND PARENT SIZE) RefreshViewSizeVariables
	//

	private Transform thisOrAltParentTransform;

	public bool RefreshViewSizeVariables()
	{
		canvas = transform.root.GetComponent<Canvas>();

		if (canvas == null)
		{
			Debug.LogWarning(@"Party: Missing CANVAS @ ROOT");
			return false;
		}

		canvasScale = canvas.transform.lossyScale;
		screenSizeSet = new Vector2(canvas.GetComponent<RectTransform>().rect.width, canvas.GetComponent<RectTransform>().rect.height);
		screenSize = screenSizeSet * canvasScale;

		thisOrAltParentTransform = this.transform;

		if (Creator.AlternateParent != null)
		{
			thisOrAltParentTransform = Creator.AlternateParent.transform;
			canvasScale = thisOrAltParentTransform.lossyScale;
		}

		//
		//

		if (transform.GetComponent<RectTransform>() == null)
		{
			Debug.LogWarning(@"Party: Missing RectTransform");
			return false;
		}
		else
		{
			parentSize = new Vector2(transform.GetComponent<RectTransform>().rect.width, transform.GetComponent<RectTransform>().rect.height);			
		}

		parentSize += new Vector2(Spawner.Border, Spawner.Border);

		return true;
	}

	//
	// PositionParticle
	//

	void PositionParticle(cParticle node)
	{
		//
		//

		//
		//

		//
		//

		Vector2 bordermod = new Vector2(0, 0);

		if (Spawner.Border != 0 && Spawner.BorderRandom)
		{
			bordermod = new Vector2(Random.Range(-Spawner.Border, Spawner.Border), Random.Range(-Spawner.Border, Spawner.Border));
		}

		if (Creator.AlternateParent != null || Creator.UseScreenLimits)
		{
			Vector2 worldOut = -transform.InverseTransformPoint(0,0,0);
			worldOut += GetOrigin;
            worldOut *= canvasScale;
            node.Obj.transform.position = worldOut + bordermod;
        }
        else
        {
			node.Obj.transform.localPosition = GetOrigin + bordermod;
		}
	}

	//
	// GetOrigin
	//

	Vector2 GetOrigin
	{
		get
		{
			Vector2 viewxy = parentSize * 0.5f;

			if (Spawner.FxArea.x != 0)
				viewxy.x = Spawner.FxArea.x;

			if (Spawner.FxArea.y != 0)
				viewxy.y = Spawner.FxArea.y;

			Vector2 xyout = new Vector2(Random.Range(-viewxy.x, viewxy.x), Random.Range(-viewxy.y, viewxy.y));

			//
			// radial
			//

			if (Spawner.RadialSpawn != Vector2.zero)
            {
				radialIndex += radialIncrement;

				// wrap index back to 0 if > 360
				//
				int outp = (int)radialIndex % Orb.Length;
				xyout += new Vector2(Spawner.RadialSpawn.x * Orb[outp].x, Spawner.RadialSpawn.y * Orb[outp].y);
			}

			//
			// corners
			//

			List<int> corners = new List<int>();

			if (Spawner.TopLeft > 0)
				corners.Add(1);

			if (Spawner.TopRight > 0)
				corners.Add(2);

			if (Spawner.BottomLeft > 0)
				corners.Add(3);

			if (Spawner.BottomRight > 0)
				corners.Add(4);

			if (corners.Count > 0)
			{
				int rc = corners[Random.Range(0, corners.Count)];

				if (rc == 1)
					xyout = new Vector2(-viewxy.x + Random.Range(0, Spawner.TopLeft), viewxy.y - Random.Range(0, Spawner.TopLeft));
				else if (rc == 2)
					xyout = new Vector2(viewxy.x - Random.Range(0, Spawner.TopRight), viewxy.y - Random.Range(0, Spawner.TopRight));
				else if (rc == 3)
					xyout = new Vector2(-viewxy.x + Random.Range(0, Spawner.BottomLeft), -viewxy.y + Random.Range(0, Spawner.BottomLeft));
				else if (rc == 4)
					xyout = new Vector2(viewxy.x - Random.Range(0, Spawner.BottomRight), -viewxy.y + Random.Range(0, Spawner.BottomRight));
			}

			//
			// clamping
			//

			List<int> clamp = new List<int>();

			if (Spawner.ClampTop)
				clamp.Add(1);

			if (Spawner.ClampBottom)
				clamp.Add(2);

			if (Spawner.ClampLeft)
				clamp.Add(3);

			if (Spawner.ClampRight)
				clamp.Add(4);

            if (clamp.Count > 0)
            {
                int rc = clamp[Random.Range(0, clamp.Count)];

                if (rc == 1)
					xyout.y = viewxy.y;
				else if (rc == 2)
                    xyout.y = -viewxy.y;
                else if (rc == 3)
                    xyout.x = -viewxy.x;
                else if (rc == 4)
                    xyout.x = viewxy.x;
            }

            return xyout;
		}
	}

	//
	// OffViewCheck
	//

	void OffViewCheck(cParticle node)
	{
		Vector3 lscale = node.Obj.transform.localScale;
		Vector3 nodexy;
		Vector3 checksize;

		//
		//

		//
		//

		//
		//

		if (Creator.AlternateParent != null || Creator.UseScreenLimits)
		{
			//checksize = new Vector2 (screenSize.x + Spawner.Border, screenSize.y + Spawner.Border);
			nodexy = node.Obj.transform.position;

			if (nodexy.y > screenSize.y + Spawner.Border || nodexy.y < -Spawner.Border || nodexy.x > screenSize.x + Spawner.Border || nodexy.x < -Spawner.Border)
			{
				node.Active = false;
			}
        }
        else
        {
			checksize = parentSize * 0.5f;
			nodexy = node.Obj.transform.localPosition;

			if (nodexy.y > checksize.y || nodexy.y < -checksize.y)
			{
				if (node.Bounce)
				{
					node.Obj.transform.localScale = new Vector3(transform.localScale.x, lscale.y = -lscale.y, transform.localScale.z);
					node.Delta.y = -node.Delta.y;
				}
				else
				{
					node.Active = false;
				}
			}
			else if (nodexy.x > checksize.x || nodexy.x < -checksize.x)
			{
				if (node.Bounce)
				{
					node.Obj.transform.localScale = new Vector3(lscale.x = -lscale.x, transform.localScale.y, transform.localScale.z);
					node.Delta.x = -node.Delta.x;
				}
				else
				{
					node.Active = false;
				}
			}
		}
	}
}