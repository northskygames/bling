﻿//
// V1.6
//
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Framer : MonoBehaviour
{
    #region VARIABLES

    private GameObject Obj;
    private Image Img = null;
    private bool initialized = false;

    const float rndhi = 1.5f;
    const float rndlo = 0.5f;

    private float AnimationSpeedRestore;
    private float AnimationSpeedSet;
    private int AnimationFrameSet;
    private float SequenceDelayRestore;
    private float SequenceDelaySet;

    [Header("ANIMATED FRAMES")]
    public Sprite[] Frame = null;
    [Header("SEQUENCES (start each with a -1)")]
    public int[] Seq = null;
    private int SeqIndex = 0;
    private List<int> SeqI = new List<int>();
    [Space(10)]
    public int CurrentFrame;
    public bool RandomFrame;
    [RangeAttribute(0, 50)]
    public float AnimationSpeed;
    [Space(10)]
    [RangeAttribute(0, 1000)]
    public float SequenceDelay;
    public bool RandomDelay;
    [Space(10)]
    public bool AnimateOnce;
    public bool AnimateOnEnable;
    public Material AnimationMaterial;
    public Color AnimationColor = Color.white;
    [Space(10)]
    public bool Active = false;

    #endregion

    void OnEnable()
    {
        StartCoroutine(KickStart());
    }

    private IEnumerator KickStart()
    {
        // wait 1 frame
        //
        yield return null;

        if (!initialized)
        {
            // if several animation sequences are needed search for them, each begins with a -1
            //
            if (Seq.Length > 0)
            {
                for (int i = 0; i < Seq.Length; i++)
                {
                    if (Seq [i] == -1)
                        SeqI.Add(i);
                }
                SeqIndex = SeqI[Random.Range(0, SeqI.Count)];
            }

            if (Frame != null && Frame.Length >= 2)
            {
                float currentfps = 1.0f / Time.fixedDeltaTime;

                // if the animation object has an image attached to it, use it. if not, create one.
                //
                if (!this.GetComponent <Image>())
                {
                    CreateAnimator();
                }
                else
                {
                    Img = this.GetComponent<Image>();
                }

                if (RandomFrame)
                    AnimationFrameSet = Random.Range(0, Frame.Length);

                else
                    AnimationFrameSet = CurrentFrame;

                if (AnimationFrameSet >= Frame.Length)
                    AnimationFrameSet = 0;

                if (Img != null && Frame[AnimationFrameSet] != null)
                    Img.sprite = Frame[AnimationFrameSet];

                AnimationSpeedSet = AnimationSpeed * currentfps;
                AnimationSpeedRestore = AnimationSpeedSet;
                SequenceDelaySet = SequenceDelay * currentfps;
                SequenceDelayRestore = SequenceDelaySet;

                HandleLoopDelayRestore();

                if (AnimateOnEnable)
                    SequenceDelaySet = 0;
            }
            initialized = true;
            Active = true;
        }
    }

    void HandleLoopDelayRestore()
    {
        if (RandomDelay)
            SequenceDelaySet = (int)Random.Range((float)SequenceDelayRestore * rndlo, (float)SequenceDelayRestore * rndhi);
        else
            SequenceDelaySet = SequenceDelayRestore;
    }

    void FixedUpdate()
    {
        if (Active)
            HandleAnimate();
    }

    void CreateAnimator()
    {
        Obj = new GameObject("animator");
        Obj.AddComponent <Image>();
        Img = Obj.GetComponent<Image>();
        Img.material = AnimationMaterial;
        Img.color = AnimationColor;
        Obj.transform.SetParent(gameObject.transform, true);
        Obj.transform.rotation = transform.rotation;
        Obj.transform.localScale = new Vector3(1, 1, 1);
        Obj.transform.localPosition = Vector2.zero;
        Obj.GetComponent<RectTransform>().pivot = GetComponent<RectTransform>().pivot;
        Obj.GetComponent<RectTransform>().sizeDelta = GetComponent<RectTransform>().rect.size;
        Obj.SetActive(true);
    }

    void AssignSpriteFrame()
    {
        if (RandomFrame)
        {
            AnimationFrameSet = Random.Range(0, Frame.Length);
        }
        else if (Seq.Length > 0)
        {
            bool getNewSequence = false;

            SeqIndex++;

            if (SeqIndex == Seq.Length)
            {
                getNewSequence = true;
            }
            else if (Seq[SeqIndex] == -1)
            {
                getNewSequence = true;
            }

            if (getNewSequence)
            {
                SeqIndex = SeqI[Random.Range(0, SeqI.Count)];
                HandleLoopDelayRestore();
            }
            else
            {
                AnimationFrameSet = Seq[SeqIndex];
                //Debug.Log("index " + SeqIndex.ToString() + " frame " + Seq[SeqIndex].ToString());
            }
        }
        else
        {
            AnimationFrameSet++;

            if (AnimationFrameSet > Frame.Length - 1)
            {
                AnimationFrameSet = 0;
                if (AnimateOnce)
                {
                    Active = false;
                    Destroy(Obj);
                }
                else
                {
                    HandleLoopDelayRestore();
                }
            }
        }

        if (AnimationFrameSet >= 0 && AnimationFrameSet < Frame.Length && Frame[AnimationFrameSet] != null && Img != null)
        {
            Img.sprite = Frame[AnimationFrameSet];
        }
        else
        {
            Active = false;
        }
    }

    void HandleAnimate()
    {
        if (SequenceDelaySet-- <= 0)
        {
            if (AnimationSpeedSet-- <= 0)
            {
                AnimationSpeedSet = AnimationSpeedRestore;
                AssignSpriteFrame();
            }
        }
    }

    public void DeleteThisObject()
    {
        Destroy(gameObject);
    }
}
