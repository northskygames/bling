﻿//
// V1.6
//
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.Events;

public class Fader : MonoBehaviour
{
	#region VARIABLES
	[System.NonSerialized]
	public GameObject Obj;
	[System.NonSerialized]
	public Color ColourSet;
	[System.NonSerialized]
	public float FadeInSet;
	[System.NonSerialized]
	public float FadeDelaySet;
	[System.NonSerialized]
	public float FadeOutSet;
	[System.NonSerialized]
	public float FadeLoopDelaySet;
	[System.NonSerialized]
	public Image Img;

	public Sprite FadeSprite;
	[RangeAttribute(0f, 50f)]
	public float FadeIn;
	public bool RandomFadeIn;
	[RangeAttribute(0f, 500f)]
	public float FadeDelay;
	public bool RandomFadeDelay;
	[RangeAttribute(0f, 50f)]
	public float FadeOut;
	public bool RandomFadeOut;
	[RangeAttribute(0f, 1000f)]
	public float FadeLoopDelay;
	public bool RandomDelay;
	public bool BurstFader;
	public Material FaderMaterial;
	public Color Colour = Color.white;
	[Space(10)]
	public bool Active;

	[Space(10)]
	public UnityEvent fadeComplete;

	// v1.6 updated the following to FALSE
	//
	private bool validFader = false;

	private bool initialized = false;
	private float alphaTarget = 1;

	const float rndhi = 1.5f;
	const float rndlo = 0.5f;
	#endregion

	void Awake () 
	{
		if (!initialized) 
		{
			StartCoroutine(KickStart());
			initialized = true;
		}
	}

	void OnEnable()
	{
		StartCoroutine(Delay());
	}

	IEnumerator Delay()
	{
		yield return new WaitForEndOfFrame();

		if (validFader)
		{
			CreateFader();
			ResetFader();
			Active = true;
		}
		else
		{
			Active = false;
		}
	}

	void OnDisable()
	{
		if (Obj != null)
			Destroy (Obj);
		Active = false;
	}

	private IEnumerator KickStart () 
	{
		yield return null;

		validFader = false;

		if (this.GetComponent<Image> () == null && FadeSprite == null)
		{
			Debug.LogWarning ("Pop: Missing Image on " + this.name);
		} else if (FadeIn != 0 || FadeOut != 0)
		{
			ConvertTimers ();
			alphaTarget = Colour.a;
			Colour.a = 0;
			ColourSet =  new Color (Colour.r, Colour.g, Colour.b, 0);
			FadeInSet = FadeIn;
			FadeDelaySet = FadeDelay;
			FadeOutSet = FadeOut;
			FadeLoopDelaySet = FadeLoopDelay;
			validFader = true;
		}
	}

	void FixedUpdate ()
	{
		if (Active)
			HandleFader ();
	}

	void CreateFader () 
	{
		Obj = new GameObject("fader");
		Obj.AddComponent <Image>();
		Img = Obj.GetComponent<Image>();
		Img.material = FaderMaterial;
		Img.color = Colour;

		if (FadeSprite != null) 
		{
			Img.sprite = FadeSprite;
			Obj.GetComponent<RectTransform>().sizeDelta = FadeSprite.rect.size;
		} else 
		{
			Img.sprite = this.GetComponent<Image>().sprite;
			Obj.transform.GetComponent<Image> ().type = this.GetComponent<Image>().type;
			Obj.GetComponent<RectTransform>().pivot = GetComponent<RectTransform>().pivot;
			Obj.GetComponent<RectTransform>().sizeDelta = GetComponent<RectTransform>().rect.size;
		}
		Obj.transform.SetParent (gameObject.transform, true);
		Obj.transform.rotation = transform.rotation;
		Obj.transform.localScale = new Vector3 (1, 1, 1);
		Obj.transform.localPosition = Vector2.zero;
		Obj.SetActive (true);
	}

	void HandleFader () 
	{
		if (FadeLoopDelay > 0) 
		{
			if (FadeLoopDelay == 1)
			{
				if (FadeInSet != 0) 
				{
					Colour.a = 0;
				} else 
				{
					Colour.a = alphaTarget;
				}
			}
			FadeLoopDelay --;
		} else 
		{
			if (FadeIn > 0)
			{
				Colour.a += FadeIn;
				if (Colour.a >= alphaTarget)
					FadeIn = 0;
			} else if (FadeDelay -- > 0)
			{
			} else if (FadeOut > 0) 
			{
				Colour.a -= FadeOut;
				if (Colour.a < 0)
				{
					ResetFader ();
					if (BurstFader)
						Active = false;
				}
			}
		}
		Img.color = Colour;
		if (!Active)
		{
			fadeComplete.Invoke();
			this.gameObject.SetActive (false);
		}
	}

	void ResetFader ()
	{
		Colour = ColourSet;
		FadeIn = FadeInSet;
		FadeDelay = FadeDelaySet;
		FadeOut = FadeOutSet;
		FadeLoopDelay = FadeLoopDelaySet;

		if (RandomFadeIn)
			FadeIn = Random.Range (FadeInSet * rndlo, FadeInSet * rndhi);

		if (RandomFadeDelay)
			FadeDelay = Random.Range (FadeDelaySet * rndlo, FadeDelaySet * rndhi);

		if (RandomFadeOut)
			FadeOut = Random.Range (FadeOutSet * rndlo, FadeOutSet * rndhi);
	}

	void ConvertTimers () 
	{
		float CurrentFPS = 1.0f / Time.deltaTime;

		if (FadeDelay != 0)
			FadeDelay *= CurrentFPS;

		if (FadeLoopDelay != 0)
			FadeLoopDelay *= CurrentFPS;

		if (FadeIn != 0) 
		{
			float frametotal = FadeIn * CurrentFPS;
			FadeIn = 1f / frametotal;
		}
		if (FadeOut != 0) 
		{
			float frametotal = FadeOut * CurrentFPS;
			FadeOut = 1f / frametotal;
		}
	}

	public void DeleteThisObject () 
	{
		Destroy (gameObject);
	}
}