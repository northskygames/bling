//
// V1.0
//
using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class Pawz : MonoBehaviour
{
    public float pauseOne;
    public float pauseTwo;
	[Space(10)]
	public bool repeat;
	[Space(10)]
	public UnityEvent pauseOver;

	private float pauseAmount;
	
	void OnEnable()
	{
		Working();
	}

	void Working()
    {
		if (pauseTwo != 0)
		{
			pauseAmount = Random.Range(pauseOne, pauseTwo);
        }
        else
        {
			pauseAmount = pauseOne;
        }

		StartCoroutine(Delay());
	}

	void AllDone()
    {
		pauseOver.Invoke();

		StopAllCoroutines();

		if (repeat)
        {
			Working();
		}
        else
        {
			// ... thats all she wrote
        }
	}

	IEnumerator Delay()
	{
		yield return new WaitForSeconds(pauseAmount);

		AllDone();
	}
}