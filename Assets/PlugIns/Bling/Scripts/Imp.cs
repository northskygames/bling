//
// V1.1
//
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class Imp : MonoBehaviour
{
	private bool initialized = false;
	private float alpha;
	public float fadeTime = 0.8f;
	public UnityEvent fadeInComplete;
	public UnityEvent fadeOutComplete;

	public enum FadeStates
	{
		Idle,
		FadeIn,
		FadeOut
	}

	public FadeStates currentState = FadeStates.Idle;

    private void Awake()
    {
        alpha = 0;
        GetComponent<CanvasGroup>().alpha = 0;

        currentState = FadeStates.Idle;
        ConvertTimers();
        initialized = true;
    }

	void ConvertTimers()
	{
		float currentfps = 1.0f / Time.fixedDeltaTime;
		float frametotal = fadeTime * currentfps;
		fadeTime = 1f / frametotal;
	}

	public void FadeIn()
    {
		currentState = FadeStates.FadeIn;
	}

	public void FadeOut()
	{
		currentState = FadeStates.FadeOut;
	}

	void FixedUpdate()
	{
		if (currentState != FadeStates.Idle)
        {
			if (currentState == FadeStates.FadeIn)
			{
				if (alpha < 1)
                {
					alpha += fadeTime;
				}
                else
                {
					alpha = 1;
					currentState = FadeStates.Idle;
					fadeInComplete.Invoke();
				}
			}
			else if (currentState == FadeStates.FadeOut)
			{
				if (alpha > 0)
				{
					alpha -= fadeTime;
				}
				else
				{
					alpha = 0;
					currentState = FadeStates.Idle;
					fadeOutComplete.Invoke();
				}
			}
			//
			// update parent alpha
			//
			GetComponent<CanvasGroup>().alpha = alpha;
		}
	}
}
