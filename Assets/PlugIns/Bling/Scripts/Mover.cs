﻿//
// V1.3
//
using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using UnityEngine.Serialization;

public class Mover : MonoBehaviour
{
    #region VARIABLES

    private bool initialized = false;
    private bool moveActive = false;
    private bool rotateActive = false;
    private bool rotatePaused = false;
    private bool maxLeft = false;
    private bool maxRight = false;
    private bool bounceIn = false;

    private float currentLerpTime;
    private float rotateBounceTime;
    private float currentFps;

    public enum EaseOptions
    {
        None,
        In,
        Out,
        InOut,
        OutIn
    };

    //
    // MOVE
    //
    [Header("MOVER V1.3")]
    [Space(10)]
    public bool useStartPosition = false;
    public Vector3 startPosition;
    private Vector3 startSet;

    //
    // endTarget = old name, endPosition = new name, FormerlySerializedAs = ensures inspector value remains intact
    //
    [FormerlySerializedAs("endTarget")]
    public Vector3 endPosition;
    private Vector2 endSet;
    public bool useLocalPosition = false;
    public float moveSpeed;
    private float moveSet;

    [SerializeField]
    private EaseOptions ease = EaseOptions.None;

    public bool bounce = false;
    public bool endAfterBounce = false;

    //
    // ROTATE
    //
    [Header("ROTATION")]
    [Space(10)]
    public float rotateBounceAngle;
    private float rotateBounceAngleSet;
    public float rotateSpeed;
    private float rotateSpeedSet;
    public bool rotateRandomiser = false;
    public float rotatePause;
    private Quaternion rotateSet;

    [Space(10)]
    public UnityEvent stoppedMoving;
    public bool active = false;

    #endregion

    void OnEnable()
    {
        //
        // * W A R N I N G *
        //
        // The option: 'use start position';
        // since the code has a minor delay (yield) upon start-up
        // we need to update this objects start position asap else...
        // it will glitch for a frame (current position) THEN relocate to its 'start position'
        //
        if (useStartPosition)
            transform.localPosition = startPosition;

        StartCoroutine(DelayInitialize());
    }

    private IEnumerator DelayInitialize()
    {
        yield return null;

        if (!initialized)
            KickStart();

        //
        // reset all start positions and rotations
        //
        moveSpeed = moveSet;
        startPosition = startSet;
        endPosition = endSet;
        transform.localPosition = startPosition;
        transform.rotation = rotateSet;

        ResetRotate();
        ResetMove();
    }

    void OnDisable ()
    {
        moveActive = false;
        rotateActive = false;
        active = false;
    }

    IEnumerator Delay()
    {
        yield return new WaitForSeconds(rotatePause);
        rotatePaused = false;
    }

    //
    // KICKSTART
    //
    void KickStart ()
    {
        currentFps = 1.0f / Time.fixedDeltaTime;

        if (useStartPosition)
            transform.localPosition = startPosition;

        startSet = transform.localPosition;

        if (!useLocalPosition)
            endSet = new Vector2(endPosition.x + transform.localPosition.x, endPosition.y + transform.localPosition.y);
        else
            endSet = endPosition;

        moveSet = moveSpeed;

        if (rotateSpeed != 0)
            rotateSpeed *= currentFps;

        rotateSpeedSet = rotateSpeed;
        rotateBounceAngleSet = rotateBounceAngle;
        rotateSet = transform.rotation;
        initialized = true;
    }

    //
    // RESET MOVE
    //
    public void ResetMove()
    {
        currentLerpTime = 0f;

        if (moveSpeed != 0)
        {
            if (bounce)
            {
                if (!bounceIn)
                {
                    endPosition = endSet;
                    startPosition = startSet;
                }
                else
                {
                    endPosition = startSet;
                    startPosition = endSet;
                }

                bounceIn = !bounceIn;
            }
            else
            {
                startPosition = startSet;
                endPosition = endSet;
            }

            moveActive = true;
            active = true;
            moveSpeed = UpdateMoveSpeed(moveSet);
        }
        else
        {
            moveActive = false;
        }
    }

    //
    // RESET ROTATE
    //
    void ResetRotate()
    {
        if (rotateSpeed != 0)
        {
            if (rotateRandomiser)
            {
                rotateBounceAngle = Random.Range(-rotateBounceAngleSet, rotateBounceAngleSet);
            }
            else
            {
                rotateBounceAngle = rotateBounceAngleSet;
            }

            rotateSpeed = rotateSpeedSet;
            rotatePaused = false;
            maxLeft = false;
            maxRight = false;
            rotateBounceTime = 0;
            rotateActive = true;
            active = true;
        }
        else
        {
            rotateActive = false;
        }
    }

    //
    // FIXED UPDATE
    //
    void FixedUpdate ()
    {
		if (active)
            HandleMover ();
	}

    //
    // HANDLE MOVER
    //
    void HandleMover()
    {
        //
        // ROTATING?
        //
        if (rotateActive && !rotatePaused)
        {
            if (rotateBounceAngle != 0)
            {
                rotateBounceTime = rotateBounceTime + Time.deltaTime;
                float phase = Mathf.Sin(rotateBounceTime / rotateSpeed);
                transform.localRotation = Quaternion.Euler(new Vector3(0, 0, phase * rotateBounceAngle));

                if (rotatePause != 0)
                {
                    int minmax = (int)(phase * rotateBounceAngle);

                    if (minmax <= -(Mathf.Abs(rotateBounceAngle) - 1) && !maxLeft)
                    {
                        maxLeft = true;
                        maxRight = false;
                        rotatePaused = true;
                        StartCoroutine(Delay());
                    }
                    else if (minmax >= (Mathf.Abs(rotateBounceAngle) - 1) && !maxRight)
                    {
                        maxLeft = false;
                        maxRight = true;
                        rotatePaused = true;
                        StartCoroutine(Delay());
                    }
                }
            }
            else
            {
                transform.Rotate(0, 0, rotateSpeed * Time.fixedDeltaTime);
            }
        }

        //
        // MOVING?
        //
        if (moveActive)
        {
            if (Vector3.Distance(transform.localPosition, endPosition) > 1f)
            {
                if (ease != EaseOptions.None)
                {
                    currentLerpTime += Time.deltaTime;

                    if (currentLerpTime > moveSpeed)
                        currentLerpTime = moveSpeed;

                    float n = currentLerpTime / moveSpeed;

                    // ease in
                    //
                    if (ease == EaseOptions.In || (ease == EaseOptions.InOut && !bounceIn) || (ease == EaseOptions.OutIn && bounceIn))
                        n = Mathf.Sin(n * Mathf.PI * 0.5f);

                    // ease out
                    //
                    if (ease == EaseOptions.Out || (ease == EaseOptions.InOut && bounceIn) || (ease == EaseOptions.OutIn && !bounceIn))
                        n = 1f - Mathf.Cos(n * Mathf.PI * 0.5f);

                    transform.localPosition = Vector3.Lerp(startPosition, endPosition, n);
                }
                else
                {
                    transform.localPosition = Vector3.MoveTowards(transform.localPosition, endPosition, moveSpeed);
                }
            }
            else
            {
                // ensure the object ends at the exact location
                //
                transform.localPosition = endPosition;

                if (bounce)
                {
                    if (!bounceIn && endAfterBounce)
                    {
                        moveActive = false;
                        rotateActive = false;
                    }
                    else
                    {
                        ResetMove();
                    }
                }
                else
                {
                    moveActive = false;
                }
            }
        }

        if (!rotateActive && !moveActive)
        {
            active = false;
            stoppedMoving.Invoke();
        }
    }

    //
    // UPDATE MOVE SPEED
    //
    float UpdateMoveSpeed(float m)
    {
        if (m < 0)
        {
            float n = -m;
            float r = n - (n / 2);
            m = Random.Range(r, n);
        }

        m = Vector3.Distance(endPosition, startPosition) / (m * currentFps);

        return m;
    }

	public void DeleteThisObject ()
    {
		Destroy (gameObject);
	}
}