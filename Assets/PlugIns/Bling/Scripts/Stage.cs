﻿//
// V1.7
//
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Stage : MonoBehaviour
{
	[System.Serializable]
	public class ClsStage
    {
		public GameObject StageEvent;

        public ClsStage(GameObject stageevent)
        {
			StageEvent = stageevent;
        }
		public ClsStage()
        {
            StageEvent = null;
		}
    }

    public bool loopStages;
    public List <ClsStage> stage = new List<ClsStage>();

	private int stageindex = 0;

    // v1.7 updated the following to FALSE
    //
    private bool stageDone = false;
    private bool initialized = false;

    private void OnEnable()
    {
        ResetStage();
        StartCoroutine(Delay());
    }

    IEnumerator Delay()
    {
        yield return new WaitForEndOfFrame();
        if (!initialized)
        {
            ResetStage();
        }
	}

    void ResetStage()
    {
        stageindex = 0;
        stageDone = false;
        initialized = true;
        foreach (ClsStage g in stage)
        {
            g.StageEvent.SetActive(false);
            if (g.StageEvent.GetComponent<CanvasGroup>())
            {
                g.StageEvent.GetComponent<CanvasGroup>().alpha = 0;
            }
        }
    }

    public void StageDone()
    {
        stageDone = true;
    }

    void HandleStage ()
    {
        ClsStage node = stage[stageindex];

        if (!node.StageEvent.activeSelf)
            node.StageEvent.SetActive(true);

        if (stageDone)
        {
            stageDone = false;
            node.StageEvent.SetActive(false);
            stageindex++;
        }

        if (stageindex >= stage.Count)
        {
            if (loopStages)
            {
                stageindex = 0;
            }
            else
            {
                stage.Clear();
                initialized = false;
            }
        }
    }

	void FixedUpdate ()
    {
		if (stage.Count > 0 && initialized)
            HandleStage ();
	}
}
